events { }
stream {
    server {
        listen 27017;
        proxy_connect_timeout 1s;
        proxy_timeout 3s;
        proxy_pass  stream_mongo_backend;
    }
    upstream stream_mongo_backend {
        server localhost:27017;
    }
}
http {
  upstream upstreamClient {
    server client:8080 fail_timeout=0;
  }
  upstream upstreamPathsanalysis {
    server pathsanalysis:8000 fail_timeout=0;
  }
  upstream upstreamAPI {
    server serverpy:5000 fail_timeout=0;
  }
  server {
    listen 443 ssl;
    listen [::]:443 ssl;
    listen 80;
    listen [::]:80;
    ssl_certificate /etc/certs/nginx.crt;
    ssl_certificate_key /etc/certs/nginx.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
    ssl_ecdh_curve secp384r1;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;

    location  ~* ^/analyse {
      try_files $uri @analysis;
    }
    location  ~* ^/dereference {
      try_files $uri @analysis;
    }
    location @analysis {
      proxy_pass http://upstreamPathsanalysis;
      proxy_read_timeout 600s;
      proxy_redirect off;
      client_body_buffer_size 50M; 
      client_max_body_size 50M;
    }
    location @frontend {
      proxy_pass http://upstreamClient;
      proxy_redirect off;
    }
    location /socket.io {
      proxy_read_timeout 600s;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_pass http://upstreamAPI/socket.io;
      proxy_http_version 1.1;
      proxy_set_header Host $host;
    }
    location ~* ^/api {
      proxy_pass http://upstreamAPI;
      proxy_read_timeout 600s;
      client_body_buffer_size 50M; 
      client_max_body_size 50M;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_set_header Host $host;
    }
    location /sockjs-node {
      proxy_pass http://upstreamClient;
      proxy_http_version 1.1; 
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_set_header Host $host;
    }
    location /__webpack_hmr {
      proxy_pass http://upstreamClient;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_set_header Host $host;
    }
    location / {
      try_files $uri @frontend;
    }
  }
}