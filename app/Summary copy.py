import pandas as pd
#from math import floor
#from math import ceil
#from datetime import datetime
from functools import reduce
from scipy import stats



class Summary:

    def __init__(self, paths, entities, selectedentities, selectedpaths):
        self.paths = paths
        self.entities = entities
        self.selectedpaths = selectedpaths
        #print('selectedentities before', selectedentities)
        self.selectedentities = self.entities
        if selectedentities:
            selectedentities = set(selectedentities) # speed up the test below
            self.selectedentities = [ent for ent in self.entities
                                     if ent['uri'] in selectedentities]

        self.checkdistribution = selectedentities is not None
        self.numericaltypes = set(["http://www.w3.org/2001/XMLSchema#integer",
            "http://www.w3.org/2001/XMLSchema#gYear",
            "http://www.w3.org/2001/XMLSchema#double"])
        self.datetimetypes = set(["http://www.w3.org/2001/XMLSchema#dateTime"])
        #print('selectedentities after', len(self.selectedentities))
        #print('entities 0', self.entities[0])
        #print('selectedentities 0', self.selectedentities[0])
        #print('selectedentities 1', self.selectedentities[1])
        self.summarisedpaths = []
        self.missing = []

    def getSummary(self, minpercentsummary):
        def summarisePath(pathindex, path):
            selectedentities = self.selectedentities.copy() #no need
            entitiespath = [entity['paths'][pathindex] for entity in selectedentities]
            #print('entitiespath', entitiespath)
            entitiesnotnull = [value for value in entitiespath if value is not None]
            #print('entitiesnotnull', entitiesnotnull)

            summaryvalues = []
            summarydatatype = []
            summarylang = []
            # summarydistribution = {}

            flatentities = []
            flatdatatypes = []
            flatlanguages = []
            uniqueValues = 0
            uniqueDatatypes = 0
            uniqueLanguages = 0
            nbdatatypesnotnull = 0
            nblanguagesnotnull = 0
            compareDistrib = { 'values': {}, 'datatypes': {}, 'languages': {} }
            ks2 = { 'values': {}, 'datatypes': {}, 'languages': {} }

            coeff = len(self.entities) / len(self.selectedentities)

            def checkDatatype(a,b):
                if b is None:
                    return 0
                else:
                    hasdatatype = 1 if [v for v in b if v[1] is not None] else 0
                    return a + hasdatatype
            nbdatatypesnotnull = reduce(checkDatatype, entitiesnotnull, 0)
            #print('nbdatatypesnotnull', nbdatatypesnotnull)
            def checkLanguage(a,b):
                if b is None:
                    return 0
                else:
                    haslanguage = 1 if [v for v in b if v[2] is not None] else 0
                    return a + haslanguage
            nblanguagesnotnull = reduce(checkLanguage, entitiesnotnull.copy(), 0)
            #print('nblanguagesnotnull', nblanguagesnotnull)

            #print('minvaluesummary',minvaluesummary)

            if not entitiesnotnull:
                def flatten(a,b):
                    a.extend(b)
                    return a
                flatentities = reduce(flatten, entitiesnotnull, [])
                minvaluesummary = 1 if len(flatentities) < 35 else len(flatentities) * minpercentsummary / 100
                flatdatatypes = [value for value in flatentities if value[1] is not None]
                flatlanguages = [value for value in flatentities if value[2] is not None]
                minvaluesummarylang = 1 if len(flatlanguages) < 35 else len(flatlanguages) * minpercentsummary / 100
                values = pd.DataFrame(flatentities)

                # counter = 0
                uniqueValues = values[0].nunique()
                uniqueDatatypes = values[1].nunique()
                uniqueLanguages = values[2].nunique()

                if flatdatatypes:
                    valuecounts = values[1].value_counts().items()
                    for key, val in values[1].value_counts().items():
                        summarydatatype.append({'value': key, 'count': val})
                    if self.checkdistribution and round(len(entitiesnotnull) / len(self.selectedentities) * 100) >= 50:
                        limitsHist =  [value['value'] for  value in summarydatatype]
                        # a = set(limitsHist)
                        # b = set(self.paths[pathindex]['compareDistrib']['datatypes']['limitsHist'])
                        countHist = []
                        for item in self.paths[pathindex]['summaryDatatypes']:
                            countHist.append(valuecounts[item['value']] * coeff if item['value'] in valuecounts else 0)
                            #print('countHist val', countHist)
                            #print('savedCountHist val', self.paths[pathindex]['countHist'])
                        ks2coeff, ks2p = stats.ks_2samp(
                            self.paths[pathindex]['compareDistrib']['datatypes']['countHist'],
                            countHist)
                        ks2['datatypes']['coeff'] = ks2coeff
                        ks2['datatypes']['p'] = ks2p
                    elif not self.checkdistribution:
                        compareDistrib['datatypes']['limitsHist'] = [value['value'] for value in summarydatatype]
                        compareDistrib['datatypes']['countHist'] = [value['count'] for value in summarydatatype]

                if flatlanguages:
                    totallang = 0
                    valuecounts = values[2].value_counts().items()
                    for key, val in values[2].value_counts().items():
                        if val >= minvaluesummarylang:
                            totallang = totallang + val
                            summarylang.append({'value': key, 'count': val})
                    if totallang < len(flatlanguages):
                        summarylang.append({'value': 'other', 'count': len(flatlanguages) - totallang})

                    if self.checkdistribution and round(len(entitiesnotnull) / len(self.selectedentities) * 100) >= 50:
                        limitsHist =  [value['value'] for value in summarylang]
                        # a = set(limitsHist)
                        # b = set(self.paths[pathindex]['compareDistrib']['languages']['limitsHist'])
                        countHist = []
                        for item in self.paths[pathindex]['summaryLanguages']:
                            countHist.append(valuecounts[item['value']] * coeff if item['value'] in valuecounts else 0)
                            #print('countHist val', countHist)
                            #print('savedCountHist val', self.paths[pathindex]['countHist'])
                        ks2coeff, ks2p = stats.ks_2samp(
                            self.paths[pathindex]['compareDistrib']['languages']['countHist'],
                            countHist)
                        ks2['languages']['coeff'] = ks2coeff
                        ks2['languages']['p'] = ks2p
                       
                    elif not self.checkdistribution:
                        compareDistrib['languages']['limitsHist'] = [value['value'] for value in summarylang]
                        compareDistrib['languages']['countHist'] = [value['count'] for value in summarylang]

                    #print('ici',values[2].value_counts().items())
                    #print('la',summarylang)


                if summarydatatype and summarydatatype[0]['value'] in self.datetimetypes:
                    values['date'] = pd.to_datetime(values[0], errors='coerce')
                    values['years'] = values['date']
                    values['months'] = values['date']
                    values['days'] = values['date']
                    values['hours'] = values['date']
                    #print(values)
                    #print('values avant', values)
                    values = values.dropna(subset=['date'])
                    values = values.set_index('hours')
                    hours = values.to_period(freq='H').index.value_counts()
                    values = values.set_index('days')
                    days = values.to_period(freq='D').index.value_counts()
                    values = values.set_index('months')
                    months = values.to_period(freq='M').index.value_counts()
                    values = values.set_index('years')
                    years = values.to_period(freq='Y').index.value_counts()
                    #print(, len(values.to_period(freq='Y').index), )
                    #if len(years) > 30 :
                    #    firstyear = floor(values.to_period(freq='Y').index[0] /10) * 10
                    #    lastyear = (ceil(values.to_period(freq='Y').index[len(values.to_period(freq='Y').index)-1] /10) * 10) - 1

                    #    decades = yearsrange / 10


                    valuecounts = []
                    #print(len(years), len(months), len(days), len(hours))
                    if len(years) > 1:
                        for period, count in years.items():
                            valuecounts.append((str(period), count))
                    elif len(months) > 1:
                        for period, count in months.items():
                            valuecounts.append((str(period), count))
                    elif len(days) > 1:
                        for period, count in days.items():
                            valuecounts.append((str(period), count))
                    else:
                        for period, count in hours.items():
                            valuecounts.append((str(period), count))
                    #print(valuecounts)
                else:
                    valuecounts = values[0].value_counts().items()

                total = 0
                for key, val in valuecounts:
                        #print(key, ',' ,val)
                    if val >= minvaluesummary or total + val == len(flatentities):
                        total = total + val
                        summaryvalues.append({'value': key, 'count': val})
                if total < len(flatentities):
                    summaryvalues.append({'value': 'other', 'count': len(flatentities) - total})

                if summarydatatype and (summarydatatype[0]['value'] in self.datetimetypes
                                        or summarydatatype[0]['value'] in self.numericaltypes):
                    summaryvalues = sorted(summaryvalues, key=lambda val: val['value'])


                if self.checkdistribution and round(len(entitiesnotnull) / len(self.selectedentities) * 100) >= 50:
                    limitsHist =  [value['value'] for value in summaryvalues]
                    # a = set(limitsHist)
                    # b = set(self.paths[pathindex]['compareDistrib']['values']['limitsHist'])
                    countHist = []
                    for item in self.paths[pathindex]['summaryValues']:
                        countHist.append(valuecounts[item['value']] * coeff if item['value'] in valuecounts else 0)
                        #print('countHist val', countHist)
                        #print('savedCountHist val', self.paths[pathindex]['countHist'])
                        ks2coeff, ks2p = stats.ks_2samp(
                            self.paths[pathindex]['compareDistrib']['values']['countHist'],
                            countHist)
                        ks2['values']['coeff'] = ks2coeff
                        ks2['values']['p'] = ks2p
                elif not self.checkdistribution:
                    compareDistrib['values']['limitsHist'] =  [value['value'] for value in summaryvalues]
                    compareDistrib['values']['countHist'] = [value['count'] for value in summaryvalues]

            #print('apres',pathindex, summarylang)
            return {
                'path': path,
                'uniqueEntitiesValues': len(entitiesnotnull),
                'uniqueEntitiesDatatypes': nbdatatypesnotnull,
                'uniqueEntitiesLanguages': nblanguagesnotnull,
                'percentValues': round(len(entitiesnotnull) / len(self.selectedentities) * 100),
                'percentDatatypes': round(nbdatatypesnotnull / len(self.selectedentities) * 100),
                'percentLanguages': round(nblanguagesnotnull / len(self.selectedentities) * 100),
                'totalValues': len(flatentities),
                'totalDatatypes': len(flatdatatypes),
                'totalLanguages': len(flatlanguages),
                'uniqueValues': uniqueValues,
                'uniqueDatatypes': uniqueDatatypes,
                'uniqueLanguages': uniqueLanguages,
                'countTotalEntities': len(self.selectedentities),
                'summaryValues': summaryvalues,
                'summaryDatatypes': summarydatatype,
                'summaryLanguages': summarylang,
                'compareDistrib': compareDistrib,
                'ks2': ks2,
            }
            #If the KS statistic is small or the p-value is high, then we cannot reject
            # the hypothesis that the distributions of the two samples are the same.

        self.summarisedpaths = [summarisePath(i, ent['path']) for i, ent in enumerate(self.paths)]
        return self.summarisedpaths

    def getMissing(self):
        self.missing = []
        #print( self.selectedpaths)
        for pathindex, fpath in enumerate(self.paths):
            path = fpath['path']
            entitiespath = [entity['paths'][pathindex] for entity in self.selectedentities]
            entitiesnull = [value is None for value in entitiespath]
            #print(path, len(self.selectedentities), len(entitiesnull), len(self.selectedentities) == len(entitiesnull), path in self.selectedpaths)
            if len(self.selectedentities) == len(entitiesnull) and path in self.selectedpaths:
                self.missing.append(path)
        return self.missing
