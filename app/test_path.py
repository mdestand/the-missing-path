from Path import Path
import sys
from rfc3986_validator import validate_rfc3986
from test_path_data import selectedentities, selectedentities11, fullpath
import json

class TestPath:
    
    def test_create_arg_path(self):
        path = Path(fullpath, 2, 100, selectedentities, 10, 30, False)
        assert path.uri and isinstance(path.uri, str) and validate_rfc3986(path.uri.replace("<", "").replace(">", ""), rule='URI_reference')

    def test_create_arg_uri(self):
        path = Path(fullpath, 2, 100, selectedentities, 10, 30, False)
        assert path.index and isinstance(path.index, int)

    def test_create_arg_counttotalentities(self):
        path = Path(fullpath, 2, 100, selectedentities, 10, 30, False)
        assert path.counttotalentities and isinstance(path.counttotalentities, int)

    def test_create_arg_selectedentities(self):
        path = Path(fullpath, 2, 100, selectedentities, 10, 30, False)
        assert isinstance(path.selectedentities, list) and len(path.selectedentities) > 0

    def test_compute_coeff(self):
        path = Path(fullpath, 2, 100, selectedentities, 10, 30, False)
        assert path.coeff == 4

    def test_compute_valuesnotnull(self):
        path = Path(fullpath, 4, 100, selectedentities, 10, 30, False)
        assert len(path.valuesnotnull) == 2

    def test_compute_datatypesnotnull(self):
        path = Path(fullpath, 6, 100, selectedentities, 10, 30, False)
        path2 = Path(fullpath, 2, 100, selectedentities, 10, 30, False)
        assert len(path.datatypesnotnull) == 4 and len(path2.datatypesnotnull) == 0

    def test_compute_languagesnotnull(self):
        path = Path(fullpath, 6, 100, selectedentities, 10, 30, False)
        path2 = Path(fullpath, 7, 100, selectedentities, 10, 30, False)
        assert len(path.languagesnotnull) == 0 and len(path2.languagesnotnull) == 3

    def test_compute_flatvalues(self):
        path = Path(fullpath, 7, 100, selectedentities, 10, 30, False)
        path2 = Path(fullpath, 4, 100, selectedentities, 10, 30, False)
        assert len(path.flatvalues) == 12 and len(path2.flatvalues) == 2

    def test_summarise_count_datatype_and_lang(self):
        path = Path(fullpath, 6, 100, selectedentities, 10, 30, False)
        path2 = Path(fullpath, 7, 100, selectedentities, 10, 30, False)
        assert path.countflatdataframe[1] == 4 and path2.countflatdataframe[2] == 12
    
    def test_summarise_datatypes(self):
        path = Path(fullpath, 5, 100, selectedentities, 10, 30, False)
        assert path.summarydatatypes.level == 'category' and path.summarydatatypes.summary == [{'value': 'http://www.w3.org/2001/XMLSchema#integer','count': 4}]
    
    def test_summarise_languages(self):
        path = Path(fullpath, 7, 100, selectedentities, 10, 30, False)
        assert path.summarylanguages.level == 'category' and path.summarylanguages.summary == [
            {'value': 'sw','count': 3},
            {'value': 'sv','count': 3},
            {'value': 'it','count': 3},
            {'value': 'sl','count': 2},
            {'value': 'ga','count': 1}
        ]

    def test_summarise_minvaluesummary(self):
        path = Path(fullpath, 7, 100, selectedentities, 10, 30, False)
        assert path.minvaluesummary == 1

    def test_summarise_values_categorical(self):
        path = Path(fullpath, 3, 100, selectedentities, 10, 30, False)
        assert path.summarylanguages.level == 'category' and path.summaryvalues.summary == [
            {'value': 'http://www.wikidata.org/entity/Q5','count': 3},
            {'value': 'http://www.wikidata.org/entity/Q7','count': 1}
        ]
    

    def test_summarise_values_category(self):
        path = Path(fullpath, 3, 100, selectedentities, 10, 30, False)
        assert path.summarylanguages.level == 'category' and path.summaryvalues.summary  == [
            {'value': 'http://www.wikidata.org/entity/Q5','count': 3},
            {'value': 'http://www.wikidata.org/entity/Q7','count': 1}
        ]


    def test_summarise_values_categorical_other(self):
        path = Path(fullpath, 1, 100, selectedentities11, 50, 9, False)
        assert path.summarylanguages.level == 'category' and path.summaryvalues.summary == [
            {'value': 'other','count': 22}
        ]

    def test_summarise_values_numberbin(self):
        path = Path(fullpath, 5, 100, selectedentities11, 5, 9, False)
        assert path.compareDistrib['values']['summary'] == [{'value': '[0, 10]','index': 0,'count': 2},{'value': '[10, 20]','index': 10,'count': 5},{'value': '[20, 30]','index': 20,'count': 3},{'value': '[70, 80]','index': 70,'count': 1}] and \
            path.compareDistrib['datatypes']['summary'] == [{'value': 'http://www.w3.org/2001/XMLSchema#integer','count': 11}] and \
            path.compareDistrib['languages']['summary'] == [] 

    def test_summarise_values_datebin(self):
        path = Path(fullpath, 5, 100, selectedentities11, 5, 9, False)
        assert path.compareDistrib['values']['summary'] == [{'value': '[0, 10]','index': 0,'count': 2},{'value': '[10, 20]','index': 10,'count': 5},{'value': '[20, 30]','index': 20,'count': 3},{'value': '[70, 80]','index': 70,'count': 1}] and \
            path.compareDistrib['datatypes']['summary'] == [{'value': 'http://www.w3.org/2001/XMLSchema#integer','count': 11}] and \
            path.compareDistrib['languages']['summary'] == [] 
    

    #def test_summarise_values_numberbin2(self):
    #    path = Path(fullpath, 5, 100, selectedentities11, 5, 9, False)
    #    assert json.dumps(path.compareDistrib['values']['summary']) == [{'value': '[0, 10]','index': 0,'count': 2},{'value': '[10, 20]','index': 10,'count': 5},{'value': '[20, 30]','index': 20,'count': 3},{'value': '[70, 80]','index': 70,'count': 1}] and \
    #        path.compareDistrib['datatypes']['summary'] == [{'value': 'http://www.w3.org/2001/XMLSchema#integer','count': 11}] and \
    #        path.compareDistrib['languages']['summary'] == [] 
    

    def test_summarise_compare(self):
        path = Path(fullpath, 5, 10, selectedentities, 5, 9, True)
        assert path.ks2['values'] ==  { 'p' : 0.22857142857142862, 'coeff': 0.75 }