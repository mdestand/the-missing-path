import numpy as np
import umap.umap_ as umap
import json
import pandas as pd
import requests
import hdbscan
from math import floor
from datetime import datetime
from functools import reduce
from scipy.spatial import Voronoi
from sklearn.cluster import KMeans

class Reduction:
    def __init__(self, paths, entities, selectedvalues, minclusters):
        self.paths = paths
        self.entities = entities
        self.minclusters = minclusters
        if len(selectedvalues) > 0:
            self.selectedvalues = selectedvalues
        else:
            #self.selectedvalues = list(map(lambda path : path['path'], self.paths[:50]))
            def checkPath(a,b):
                #print(b)
                #print(b['path'], b['uniqueEntitiesValues'], b['countTotalEntities'])
                if len(a) < 20 and b['uniqueEntitiesValues'] < b['countTotalEntities']:
                    a.append(b['path'])
                return a
            self.selectedvalues = reduce(checkPath, self.paths, [])
            #print(self.selectedvalues)
        def findIndex(uri):
            for ind, path in enumerate(self.paths):
                if path['path'] == uri:
                    return ind
        self.selectedIndexes = list(map(lambda path : findIndex(path), self.selectedvalues))
        self.vectors = []
        self.centerzones = []
        self.matrix_data = []


            

    def preparePaths(self):
        self.matrix_data = []
        for entity in self.entities:
            thedata = []
            for pathindex in self.selectedIndexes:
                thedata.append(entity['paths'][pathindex] is None)
            self.matrix_data.append(thedata)


    def preparePath(self, path):
        pathIndex = self.selectedvalues.index(path)
        self.matrix_data = []
        for entity in self.entities:
            thedata = []
            pathvalues = [] if entity['paths'][pathIndex] is None else list(map(lambda val : None if val is None else val[0], entity['paths'][pathIndex]))
            for index in range(len(self.selectedvalues)):
                value = self.selectedvalues[index]
                thedata.append(value in pathvalues)
            self.matrix_data.append(thedata)
            

    def getVectors(self, distanceFn):
        reducer = umap.UMAP(metric=distanceFn)
        reducer.fit_transform(self.matrix_data)
        self.vectors = reducer.embedding_.tolist()
        zones = hdbscan.HDBSCAN(min_samples=self.minclusters).fit_predict(reducer.embedding_)
        embeddings = pd.DataFrame(reducer.embedding_)
        embeddings[2] = zones
        #kmeans = KMeans(n_clusters=2, random_state=0).fit(self.matrix_data)
        #embeddings[2] = kmeans.cluster_centers_
        res = embeddings.groupby([2]).mean()

        centers = []
        for zone in res.index:
            zoneid = []
            for coord in res:
                zoneid.append(float(res[coord][zone]))
            centers.append(zoneid)
        #print(centers)
        self.centerzones = centers
        print(self.centerzones)
        #print(len(self.vectors), self.vectors)
