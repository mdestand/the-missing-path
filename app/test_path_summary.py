from PathSummary import PathSummary
import pandas as pd


class TestPathSummary:
    
    def test_create_arg(self):
        series = pd.Series([12,334,55,666,34])
        ps = PathSummary('values', series, 5, 1, 35, 'number')
        assert ps.summarytype == 'values' and ps.series.all() == series.all() and  ps.minvaluesperitem == 1 and  ps.maxitems == 35 and ps.maintype == 'number'

    def test_summarise_number(self):
        series = pd.Series([12,334,12,666,34])
        ps = PathSummary('values', series, 5, 1, 35, 'number')
        assert ps.level == 'numbercategory' and ps.summary == [
            {'count': 2,'value': '12', 'index': 12},
            {'count': 1,'value': '34', 'index': 34},
            {'count': 1,'value': '334', 'index': 334},
            {'count': 1,'value': '666', 'index': 666}
        ]
    
    def test_summarise_number2(self):
        series = pd.Series([12,334,55,666,34])
        ps = PathSummary('values', series, 5, 5, 4, 'number')
        assert ps.level == 'number' and ps.summary == [
            {'count': 3, 'index': 10, 'value': '[10, 110]'},
            {'count': 1, 'index': 310, 'value': '[310, 410]'},
            {'count': 1, 'index': 610, 'value': '[610, 710]'}
        ]

    def test_summarise_date(self):
        series = pd.Series(["2020-01-24T02:50:10Z","1962-04-02T00:00:00Z","2020-01-24T02:50:10Z","1962-04-02T00:00:00Z","1962-04-02T00:00:00Z"])
        ps = PathSummary('values', series, 5, 1, 35, 'date')
        assert ps.level == 'datecategory' and ps.summary == [
            {'count': 3,'value': '1962-04-02T00:00:00Z'},
            {'count': 2,'value': '2020-01-24T02:50:10Z'}
        ]

    def test_summarise_date2(self):
        series = pd.Series(["2020-01-24T02:50:10Z","1962-04-02T00:00:00Z","2020-01-24T02:50:10Z","1962-04-02T00:00:00Z","1962-04-02T00:00:00Z"])
        ps = PathSummary('values', series, 5, 1, 4, 'date')
        assert ps.level == 'datecategory' and ps.summary == [
            {'count': 3,'value': '1962-04-02T00:00:00Z'},
            {'count': 2,'value': '2020-01-24T02:50:10Z'}
        ]
    
    def test_summarise_date3(self):
        series = pd.Series(["2020-01-24T02:50:10Z","1962-04-02T00:00:00Z","2021-01-24T02:50:10Z","2022-04-02T00:00:00Z","2023-04-02T00:00:00Z"])
        ps = PathSummary('values', series, 5, 1, 4, 'date')
        assert ps.level == 'years' and ps.summary == [
            {'count': 1,'value': '1962'},
            {'count': 1,'value': '2020'},
            {'count': 1,'value': '2021'},
            {'count': 1,'value': '2022'},
            {'count': 1,'value': '2023'}
        ]

    def test_summarise_categories(self):
        series = pd.Series(['a','be','ceee','a','be'])
        ps = PathSummary('values', series, 5, 1, 4, 'category')
        assert ps.level == 'category' and ps.summary == [
            {'count': 2, 'value': 'be'},
            {'count': 2, 'value': 'a'},
            {'count': 1, 'value': 'ceee'}
        ]

    def test_summarise_categories2(self):
        series = pd.Series(['a','be','ceee','a','be'])
        compare = {'level': 'category', 'summary': [
            {'count': 12, 'value': 'be'},
            {'count': 4, 'value': 'a'},
            {'count': 3, 'value': 'tralala'},
            {'count': 6, 'value': 'other'}
        ]}
        ps = PathSummary('values', series, 5, 1, 4, 'category', compare)
        ps.compare()
        assert ps.newcount == [2,2,0,1] and ps.ks2['p'] == 0.028571428571428577 and ps.ks2['coeff'] == 1
