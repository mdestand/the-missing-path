#!flask/bin/python
from Reduction import Reduction 
from Selection import Selection 
from Summary import Summary 
from Popularity import Popularity
import json
import os
import copy 
import datetime
import time
from flask import Flask, jsonify, abort, request, make_response, url_for
from flask_cors import CORS, cross_origin
from flask_socketio import SocketIO
import requests
from pymongo import MongoClient
import datetime
from dotenv import load_dotenv
load_dotenv()
import pandas as pd
import eventlet
eventlet.monkey_patch()
async_mode = "eventlet"

app = Flask(__name__)
CORS(app)
socketio = SocketIO(app, logger=True, async_mode=async_mode, cors_allowed_origins="*")

#@app.errorhandler(400)
#@cross_origin()
#def bad_request(error):
#    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
@cross_origin()
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.route('/')
@cross_origin()
def send_warning():
    return "You must specify a full address"

@socketio.on('message')
@cross_origin()
def handle_message(message):
    print('received message: ' + message)
    return json.dumps({'message': message})

@socketio.on('getSummaryWS')
@cross_origin()
def getSummaryWS(form):
    #print('received my event: ',  form['identifier'], datetime.datetime.now())
    identifier = form['identifier']
    minpercentsummary = form['minpercentsummary'] if 'minpercentsummary' in form else 5
    selectedentities = form['selectedentities']
    prefixes = form['prefixes']  if 'prefixes' in form else {}
    key = form['key']

    paths = readFile("fullpaths", identifier)
    entities = readFile("entitiespathsummary", identifier)

    summary = Summary(copy.deepcopy(paths), copy.deepcopy(entities), None if len(selectedentities)==0 else selectedentities, None, prefixes)
    start = 0
    group = 1
    end = start + group if start + group < len(paths) else len(paths) - 1
    entitiessummary = summary.getSummary(minpercentsummary, start, end)
    socketio.emit('summary_ws', json.dumps({'summary': entitiessummary, 'key': key, 'total': len(paths)}))
    time.sleep(0.05)
    while end < len(paths):
        start = start + group
        #if start > 70:
        #    group = 100
        end = start + group if start + group < len(paths) else len(paths)
        #socketio.emit('summaryWS', json.dumps({'summary': [], 'key': key, 'start': start, 'end': end}))
        entitiessummary = summary.getSummary(minpercentsummary, start, end)
        socketio.emit('summary_ws', json.dumps({'summary': entitiessummary, 'key': key, 'start': start, 'end': end}))
        time.sleep(0.05)
    
    time.sleep(1)
    socketio.emit('end_summary_ws', key)

    return  json.dumps({'summary': summary.summarisedpaths})


#http://localhost:5000/load/maxplanck
@app.route('/api/load/<string:identifier>', methods = ['GET','POST'])
@cross_origin()
def loadReduction(identifier):    
    if request.method == 'GET':
        minpercentsummary = 2.5
        selectedpaths = []
        preflanguages = []
    if request.method == 'POST':
        form = request.get_json()
        selectedpaths = form['selectedpaths'] if 'selectedpaths' in form else []
        preflanguages = form['preflanguages'] if 'preflanguages' in form else ['en']
        minpercentsummary = form['minpercentsummary'] if 'minpercentsummary' in form else 5
        minclusters = form['minclusters'] if 'minclusters' in form else 5
        prefixes = form['prefixes'] if 'prefixes' in form else {}

    preflanguagesdict = {}
    for ind, lang in enumerate(preflanguages):
        preflanguagesdict[lang] = ind

    pathsfile = readFile2("fullpaths", identifier)
    paths = pathsfile["fullpaths"]
    zones = pathsfile["zones"] if "zones" in pathsfile else {}
    entities = readFile("entitiespathsummary", identifier)
    
    indexlabel = None
    for index, path in enumerate(paths):
        if path['path']  == '<http://www.w3.org/2000/01/rdf-schema#label>' or path['path']  == '<product_name>':
            indexlabel = index
        #if 'prefixed' not in path:
        #    prefixed = path['path'].replace('<','').replace('>','')
        #    for pref in prefixes:
        #        if prefixed.find(prefixes[pref]) == 0 :
        #            prefixed = prefixed.replace(prefixes[pref], pref+':')
        #            break;
        #    paths[index]['prefixed'] = prefixed 


    #if False:
    if 'vectors' in entities[0] and 'reduction' in paths[0]:
        def getEntityDesc(entity, index):
            desc = [entity['uri'], entity['vectors'][0], entity['vectors'][1]]
            label = getLabelPrefLanguage(entity['paths'][indexlabel], preflanguagesdict) if entity['paths'][indexlabel] is not None else ''
            desc.extend([label, entity['popularity'] if 'popularity' in entity else 0])
            return desc

        entities = list(map(lambda args: getEntityDesc(args[1],args[0]), enumerate(entities)))
        pathsummary = paths
        
    else:
        distancefn = 'russellrao'
        #distancefn = 'sokalmichener'

        summary = Summary(copy.deepcopy(paths), copy.deepcopy(entities), None, None)
        summary.getSummary(minpercentsummary)
        pathsummary = summary.summarisedpaths
        #print (pathsummary[0])
        selectedpaths = list(filter(lambda ent: ent['reduction'] if 'reduction' in ent else False, pathsummary))
        #print (selectedpaths)

        reduction = Reduction(copy.deepcopy(pathsummary), copy.deepcopy(entities), selectedpaths, minclusters)
        reduction.preparePaths()
        reduction.getVectors(distancefn)
        zones['centerzones'] = reduction.centerzones

        def getEntityDesc(entity, index):
            desc = [entity['uri']]
            desc.extend(reduction.vectors[index])
            label = getLabelPrefLanguage(entity['paths'][indexlabel], preflanguagesdict) if entity['paths'][indexlabel] is not None else ''
            desc.extend([label, entity['popularity'] if 'popularity' in entity else 0])
            return desc

        entities = list(map(lambda args: getEntityDesc(args[1],args[0]), enumerate(reduction.entities)))
        #for index, entity in enumerate(entities):
        #    entities[index].extend(reduction.vectors[index])
        #print(entities)

        for index, path in enumerate(pathsummary):
            pathsummary[index]['reduction'] = True if path['path'] in reduction.selectedvalues else False
            pathsummary[index]['color'] = True if len(path['summaryValues']['summary'])>1 else False

    #for index, entity in enumerate(entities):
    #    entities[index].extend(reduction.vectors[index])
    #print(zones)

    return jsonify({'entities': entities, 'paths': pathsummary, 'zones': zones })


#http://localhost:5000/reduction/maxplanck/allpaths
@app.route('/api/reduction/<string:identifier>/<path:path>', methods = ['GET', 'POST'])
@cross_origin()
def getReduction(identifier, path):
    if request.method == 'GET':
        selectedpaths = []
        minpercentsummary = 2.5
        preflanguages = []
    if request.method == 'POST':
        form = request.get_json()
        selectedpaths = form['selectedpaths']
        preflanguages = form['preflanguages'] if 'preflanguages' in form else ['en']
        minpercentsummary = form['minpercentsummary'] if 'minpercentsummary' in form else 5
        minclusters = form['minclusters'] if 'minclusters' in form else 5

    preflanguagesdict = {}
    for ind, lang in enumerate(preflanguages):
        preflanguagesdict[lang] = ind

    paths = readFile("fullpaths", identifier)
    entities = readFile("entitiespathsummary", identifier)
    entitiestosave = copy.deepcopy(entities)

    distancefn = 'russellrao'
    #distancefn = 'sokalmichener'
    
    summary = Summary(copy.deepcopy(paths), copy.deepcopy(entities), None, None)
    summary.getSummary(minpercentsummary)
    pathsummary = summary.summarisedpaths

    reduction = Reduction(copy.deepcopy(paths), copy.deepcopy(entities), selectedpaths, minclusters)
    if path == 'allpaths/' or path == 'allpaths':
        reduction.preparePaths()
    else:
        path = path.replace('<', '').replace('>', '')
        reduction.preparePath(path)
    reduction.getVectors(distancefn)

    indexlabel = None
    for index, path in enumerate(paths):
        if path['path']  == '<http://www.w3.org/2000/01/rdf-schema#label>' or path['path']  == '<product_name>':
            indexlabel = index

    newentities = []
    for index, entity in enumerate(entities):
        theent = [entity['uri'], reduction.vectors[index][0], reduction.vectors[index][1], getLabelPrefLanguage(entity['paths'][indexlabel], preflanguagesdict) if entity['paths'][indexlabel] is not None else '', entity['popularity'] if 'popularity' in entity else 0]
        newentities.append(theent)

    for index, path in enumerate(pathsummary):
        pathsummary[index]['reduction'] = True if path['path'] in reduction.selectedvalues else False
        pathsummary[index]['color'] = True if len(path['summaryValues']['summary'])>1 else False

    return jsonify({'entities': newentities, 'paths': pathsummary, 'zones': { 'center': reduction.centerzones } })


@app.route('/api/save/<string:identifier>', methods = ['POST'])
@cross_origin()
def saveConfig(identifier):
    form = request.get_json()
    #print(json.loads(request.form.get('retrieve_url')))
    #print(request.form.getlist('retrieve_url'))
    paths = form['paths']
    zones = form['zones']
    entities = form['entities']

    entitiestosave = readFile("entitiespathsummary", identifier)

    for index, ent in enumerate(entitiestosave):
        entitiestosave[index]['vectors'] = [entities[index][1], entities[index][2]]


    writeFile('entitiespathsummary', identifier, entitiestosave)
    writeFile2('fullpaths', identifier, {'fullpaths': paths, 'zones': zones})

    #print(entitiessummary)
    return "ok"


@app.route('/api/summary/<string:identifier>', methods = ['POST'])
@cross_origin()
def getSummary(identifier):
    form = request.get_json()
    #print(json.loads(request.form.get('retrieve_url')))
    #print(request.form.getlist('retrieve_url'))
    minpercentsummary = form['minpercentsummary'] if 'minpercentsummary' in form else 1
    selectedentities = form['entities']
    selectedpath = form['selectedpath'] if 'selectedpath' in form else None

    paths = readFile("fullpaths", identifier)
    entities = readFile("entitiespathsummary", identifier)

    if len(selectedentities)>0:
        summary = Summary(copy.deepcopy(paths), copy.deepcopy(entities), selectedentities, None)
        if selectedpath:
            entitiessummary = summary.getValuesPath(selectedpath)
        else:
            entitiessummary = summary.getSummary(minpercentsummary)
    else:
        entitiessummary = []
    #print(entitiessummary)
    return jsonify({'summary': entitiessummary})


@app.route('/api/log', methods = ['GET', 'POST'])
@cross_origin()
def setLog():
    if request.method == 'POST':
        form = request.get_json()
        action = form['action']
        user = form['user']
        content = form['content']        
        log = {"action": action, "user": user, "content": content, "date": datetime.datetime.now()}
        client = MongoClient('mongodb://' + os.getenv("VUE_APP_SERVER") + ':27017')
        client.missingpath.logs.insert_one(log)
        client.close()
        #print(log_id)
    return 'ok'


#http://localhost:5000/reduction/maxplanck/allpaths
@app.route('/api/selection/<string:identifier>', methods = ['POST'])
@cross_origin()
def getSelection(identifier):
    if request.method == 'POST':
        form = request.get_json()
        conditions = form['conditions']
        subset = form['subset'] if 'subset' in form else []
        missing = form['missing'] if 'missing' in form else False
        saveDefault = form['saveDefault'] if 'saveDefault' in form else False
        selectedpaths = form['selectedpaths'] if 'selectedpaths' in form else []

    #print('params',conditions)
    paths = readFile("fullpaths", identifier)
    entities = readFile("entitiespathsummary", identifier)

    if missing:
        summary = Summary(copy.deepcopy(paths), copy.deepcopy(entities), subset, selectedpaths)
        pathsmissing = summary.getMissing()
        #print(pathsmissing)
        return jsonify({ 'missing': pathsmissing })
    else:
        selection = Selection(copy.deepcopy(paths), copy.deepcopy(entities), conditions, subset)
        selectedentities = selection.getSelectedEntities()
        return jsonify({ 'entities': selectedentities })


@app.route('/api/values/<string:identifier>', methods = ['POST'])
@cross_origin()
def getValues(identifier):
    if request.method == 'POST':
        form = request.get_json()
        pathindex = form['pathindex']
    entities = readFile("entitiespathsummary", identifier)
    values = list(map(lambda entity:entity['paths'][pathindex], entities))
    return jsonify({ 'values': values })


#http://localhost:5000/popularity/Band
@app.route('/api/popularity/<string:identifier>', methods = ['GET', 'POST'])
@cross_origin()
def getPopularity(identifier):
    entities = readFile("entitiespathsummary", identifier)
    popularity = Popularity(copy.deepcopy(entities))
    entities = popularity.extract()
    writeFile('entitiespathsummary', identifier, entities)
    return jsonify({ 'entitiespathsummary': entities })
    #if request.method == 'GET':
        #"""return the information for <user_id>"""
    #if request.method == 'POST':


@app.route('/api/analysis/<string:identifier>', methods = ['POST'])
@cross_origin()
def makeAnalysis(identifier):
    form = request.get_json()
    maxdepth = form['maxdepth']
    endpoint = form['endpoint']
    pred = form['pred'] if 'pred' in form else None
    obj = form['obj'] if 'obj' in form else None
    constraint = form['constraint'] if 'constraint' in form else None
    step = form['step']
    step2 = form['step2']
    maxquota = form['maxquota'] if 'maxquota' in form else None
    minquota = form['minquota'] if 'minquota' in form else None
    maxvaluessummary = form['maxvaluessummary'] if 'maxvaluessummary' in form else None


    #data = requests.post(os.getenv("VUE_APP_PROTOCOL") + '://' + os.getenv("VUE_APP_SERVER") + ':80/analyse', form)
    data = requests.post( 'http://' + os.getenv("VUE_APP_SERVER") + '/analyse', form)
    #print(data)
    jsondata = data.json()
    #print(jsondata)
    fullpaths = jsondata['fullpaths']
    entitiespathsummary = jsondata['entitiespathsummary']
    writeFile('fullpaths', identifier, fullpaths)
    writeFile('entitiespathsummary', identifier, entitiespathsummary)
    return 'ok'

@app.route('/api/openfoodfacts/<string:identifier>', methods = ['POST'])
@cross_origin()
def makeOFFAnalysis(identifier):
    form = request.get_json()
    data = requests.post( 'http://' + os.getenv("VUE_APP_SERVER") + '/openfoodfacts', form)
    #print(data)
    jsondata = data.json()
    if jsondata:
        fullpaths = jsondata['fullpaths']
        entitiespathsummary = jsondata['entitiespathsummary']
        writeFile('fullpaths', identifier, fullpaths)
        writeFile('entitiespathsummary', identifier, entitiespathsummary)
    return 'ok'


def getLabelPrefLanguage(values, preflanguages):
    values = sorted(values, key=lambda val: preflanguages[val[2]] if val[2] in preflanguages else len(preflanguages)+1)
    return values[0][0]

def getLabelPrefLanguageOld(values, preflanguages):
    df = pd.DataFrame(values)
    if len(preflanguages) == 0:
        preflanguages = ['en']
    indexes = []
    for lang in preflanguages:
        indexes = df[df[2]==lang].index.values
        if len(indexes) > 0:
            break
    if len(indexes) > 0:
        return values[indexes[0]][0]
    else:
        return values[0][0]


def readFile(typecontent, identifier):
    with open("files/" + identifier + "_" + typecontent + ".json") as datafile:
        data = json.load(datafile)
        return data[typecontent]

def readFile2(typecontent, identifier):
    with open("files/" + identifier + "_" + typecontent + ".json") as datafile:
        data = json.load(datafile)
        return data

def writeFile2(typecontent, identifier, content):
    file = "files/" + identifier + "_" + typecontent + ".json"
    if os.path.isfile(file):
        os.chmod(file, 436)
        os.remove(file)
    with open(file, 'w') as outfile:
        json.dump(content, outfile)

def writeFile(typecontent, identifier, content):
    file = "files/" + identifier + "_" + typecontent + ".json"
    if os.path.isfile(file):
        os.chmod(file, 436)
        os.remove(file)
    with open(file, 'w') as outfile:
        json.dump({str(typecontent): content}, outfile)

if __name__ == '__main__':
    socketio.run( app , host='0.0.0.0',  port=5000, debug=True)
