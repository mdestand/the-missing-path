from Selection import Selection
import pandas as pd
from test_path_data import selectedentities, selectedentities11, entrypaths, comparedpaths, summarisedpaths
import json

class TestSelection:
    
    def test_create_selection_datefull(self):
        selection = Selection(summarisedpaths, selectedentities11, [["<http://schema.org/dateModified>", "values", "2019-06-29T22:53:44Z", False, False, None]], [])
        selectedentities = selection.getSelectedEntities()
        assert selectedentities == ['http://www.wikidata.org/entity/Q58426937']
    
    def test_create_selectio_date_insubset(self):
        selection = Selection(summarisedpaths, selectedentities11, [["<http://schema.org/dateModified>", "values", "2019-06-29T22:53:44Z", False, True, None]], ['http://www.wikidata.org/entity/Q58426937'])
        selectedentities = selection.getSelectedEntities()
        assert selectedentities == ['http://www.wikidata.org/entity/Q58426937']


    def test_create_selectio_date_notinsubset(self):
        selection = Selection(summarisedpaths, selectedentities11, [["<http://schema.org/dateModified>", "values", "2019-06-29T22:53:44Z", False, True, None]], ['http://www.wikidata.org/entity/Q58437'])
        selectedentities = selection.getSelectedEntities()
        assert selectedentities == []


