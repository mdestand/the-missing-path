from Summary import Summary
import pandas as pd
from test_path_data import selectedentities, selectedentities11, entrypaths, comparedpaths, summarisedpaths
import json

class TestSummary:
    
    def test_create_summary(self):
        summary1 = Summary(summarisedpaths, selectedentities, None, None)
        summary1.getSummary(10)
        assert summary1.summarisedpaths == summarisedpaths


    #def test_create_summary(self):
    #    summary1 = Summary(summarisedpaths, selectedentities11, None, None)
    #    summary1.getSummary(95)
    #    assert json.dumps(summary1.summarisedpaths) == summarisedpaths

    #def test_json_summary(self):
    #    summary1 = Summary(summarisedpaths, selectedentities, None, None)
    #    summary1.getSummary(10)
    #    assert json.dumps(summary1.summarisedpaths) == json.dumps(summarisedpaths)

    #

    def test_create_summary2(self):
        summary1 = Summary(summarisedpaths, selectedentities, ['http://www.wikidata.org/entity/Q18205300', 'http://www.wikidata.org/entity/Q58291909', 'http://www.wikidata.org/entity/Q737036'], None)
        summary1.getSummary(10)
        
        #assert summary1.summarisedpaths == comparedpaths

