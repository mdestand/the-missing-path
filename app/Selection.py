import numpy as np
import pandas as pd
import requests
from math import floor
from datetime import datetime
from dateutil.parser import parse

class Selection:
    def __init__(self, paths, entities, conditions, subset):
        self.paths = paths
        self.entities = entities
        self.conditions = conditions
        self.subset = subset
        #self.selectedpath = selectedpath
        #self.querytype = querytype
        #self.selectedvalue = selectedvalue
        self.selectedentities = []
        self.numericaltypes = [
            "http://www.w3.org/2001/XMLSchema#integer",
            "http://www.w3.org/2001/XMLSchema#gYear",
            "http://www.w3.org/2001/XMLSchema#double"
        ]
        self.datetimetypes = [
            "http://www.w3.org/2001/XMLSchema#dateTime"
        ]

    def getSelectedEntities(self):        
        #print('querytype', self.querytype)
        #print('selectedpath', self.selectedpath)
        #print('pathindex', pathindex)
        #print('selectedvalue', self.selectedvalue)
        for entity in self.entities:
            entity_ok = set()
            for condition in self.conditions:
            # print(condition)
                selectedpath = condition[0]
                querytype = condition[1]
                selectedvalue = condition[2]
                inverse = condition[3]
                subset = condition[4]
                if subset == True and entity['uri'] not in self.subset:
                    entity_ok.add(False)
                else:
                    #print('params',selectedpath, querytype, selectedvalue, inverse)
                    pathindex = None
                    for index, path in enumerate(self.paths):
                        #print(path, index)
                        if selectedpath == path['path']:
                            pathindex = index
                    if pathindex != None:
                        path_exists = entity['paths'][pathindex] != None
                        if querytype == 'path':                         
                            if (inverse == False and path_exists == True) or (inverse == True and path_exists == False):
                                entity_ok.add(True)
                            else:
                                entity_ok.add(False)
                        else:
                            indextocheck = 0
                            if querytype == 'values':
                                indextocheck = 0
                            elif querytype == 'datatypes':
                                indextocheck = 1
                            elif querytype == 'languages':
                                indextocheck = 2
                            #
                            #print('indextocheck', indextocheck)
                            value_exists = False
                            if path_exists:
                                for value in entity['paths'][pathindex]:
                                    #print ('a',selectedvalue,value[indextocheck],type(selectedvalue)) 
                                    if indextocheck == 0 and len(self.paths[pathindex]['summaryDatatypes']['summary'])> 0 and self.paths[pathindex]['summaryDatatypes']['summary'][0]['value'] in self.datetimetypes:
                                        dt = parse(value[indextocheck])
                                        if ((len(selectedvalue) == 20 and dt.strftime('%Y-%m-%dT%H:%M:%SZ') == selectedvalue) or 
                                        (len(selectedvalue) == 16 and dt.strftime('%Y-%m-%d %H:00') == selectedvalue) or 
                                        (len(selectedvalue) == 10 and dt.strftime('%Y-%m-%d') == selectedvalue) or
                                        (len(selectedvalue) == 7 and dt.strftime('%Y-%m') == selectedvalue) or
                                        (len(selectedvalue) == 4 and dt.strftime('%Y') == selectedvalue)):
                                            value_exists = True
                                    elif isinstance(selectedvalue, list):
                                        #print ('b',float(value[indextocheck]),selectedvalue)
                                        #print ('c',float(selectedvalue[0]),float(selectedvalue[1]))
                                        #print('d',float(value[indextocheck]) >= float(selectedvalue[0]) and float(value[indextocheck]) <= float(selectedvalue[1])) 
                                        if float(value[indextocheck]) >= float(selectedvalue[0]) and float(value[indextocheck]) <= float(selectedvalue[1]):
                                            value_exists = True
                                    else:
                                        #print (selectedvalue, value[indextocheck])
                                        if str(selectedvalue) == 'other':
                                            isother = True
                                            for notothervalue in self.paths[pathindex]['summaryValues']['summary']:
                                                if str(notothervalue['value']) == str(value[indextocheck]):
                                                    isother = False
                                            if isother == True:
                                                value_exists = True
                                        elif str(value[indextocheck]) == str(selectedvalue):
                                            value_exists = True
                                        #
                            #print ('e',inverse,value_exists) 
                            if (inverse == False and value_exists == True) or (inverse == True and value_exists == False):
                                entity_ok.add(True)
                            else :
                                entity_ok.add(False)
            
            #print (entity['uri'], entity_ok)
            if False not in entity_ok: 
                self.selectedentities.append(entity['uri'])
            
                            #print('value', self.selectedvalue, 'URI', entity['uri'])
        return self.selectedentities

