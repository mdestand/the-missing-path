fullpath = {
    "color": False, 
    "compareDistrib": { "datatypes":  {'summary': [{'value': 'http://www.w3.org/2001/XMLSchema#integer','count': 11}], 'level': 'category' }, "languages":  {'summary': [], 'level': '' }, "values":  {'summary': [{'value': '[0, 10]','index': 0,'count': 2},{'value': '[10, 20]','index': 10,'count': 5},{'value': '[20, 30]','index': 20,'count': 3},{'value': '[70, 80]','index': 70,'count': 1}], 'level': 'category' }}, 
    "countTotalEntities": 4567, 
    "ks2": { "datatypes": {}, "languages": {}, "values": {}}, 
    "path": "<http://www.wikidata.org/prop/P31>", 
    "percentDatatypes": 0, 
    "percentLanguages": 0, 
    "percentValues": 100, 
    "reduction": False, 
    "summaryDatatypes": [], 
    "summaryLanguages": [], 
    "summaryValues": [{ "count": 9811, "value": "other"}], 
    "totalDatatypes": 0, 
    "totalLanguages": 0, 
    "totalValues": 9811, 
    "uniqueDatatypes": 0, 
    "uniqueEntitiesDatatypes": 0,
    "uniqueEntitiesLanguages": 0, 
    "uniqueEntitiesValues": 4567, 
    "uniqueLanguages": 0, 
    "uniqueValues": 4906, 
    "prefixed": "p:P31"
}

entrypaths = [
    {
        "path": "<http://www.wikidata.org/prop/P31>", 
        "reduction": False, "color": False
    }, {
        "path": "<http://www.wikidata.org/prop/direct/P31>", 
        "reduction": False, "color": True
    }, {
        "path": "<http://schema.org/version>", 
        "reduction": False, "color": True
    }, {
        "path": "<http://schema.org/dateModified>",
        "reduction": False, "color": False
    }, {
        "path": "<http://www.w3.org/2000/01/rdf-schema#label>", 
        "reduction": False, "color": False
    }, {
        "path": "<http://wikiba.se/ontology#statements>", 
        "reduction": False, "color": True
    }, {
        "path": "<http://wikiba.se/ontology#sitelinks>", 
        "reduction": False, "color": True
    }, {
        "path": "<http://wikiba.se/ontology#identifiers>", 
        "reduction": False, "color": True
    }
]

#entrypaths = [
    #{
    #    "path": "<http://www.wikidata.org/prop/P31>", 
    #    "uniqueEntitiesValues": 142, 
    #    "uniqueEntitiesDatatypes": 0, 
    #    "uniqueEntitiesLanguages": 0, 
    #    "percentValues": 100, 
    #    "percentDatatypes": 0, 
    #    "percentLanguages": 0, 
    #    "totalValues": 217, "totalDatatypes": 0, "totalLanguages": 0, "uniqueValues": 217, "uniqueDatatypes": 0, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [{"value": "other", "count": 217}], "summaryDatatypes": [], "summaryLanguages": [], "summaryDistribution": {}, "compareDistrib": {"values": {"limitsHist": ["other"], "countHist": [217]}, "datatypes": {}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": false}, {"path": "<http://www.wikidata.org/prop/direct/P31>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 0, "uniqueEntitiesLanguages": 0, "percentValues": 100, "percentDatatypes": 0, "percentLanguages": 0, "totalValues": 217, "totalDatatypes": 0, "totalLanguages": 0, "uniqueValues": 17, "uniqueDatatypes": 0, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [{"value": "http://www.wikidata.org/entity/Q47461344", "count": 70}, {"value": "http://www.wikidata.org/entity/Q87167", "count": 66}, {"value": "http://www.wikidata.org/entity/Q56753859", "count": 38}, {"value": "http://www.wikidata.org/entity/Q2353983", "count": 17}, {"value": "other", "count": 26}], "summaryDatatypes": [], "summaryLanguages": [], "summaryDistribution": {}, "compareDistrib": {"values": {"limitsHist": ["http://www.wikidata.org/entity/Q47461344", "http://www.wikidata.org/entity/Q87167", "http://www.wikidata.org/entity/Q56753859", "http://www.wikidata.org/entity/Q2353983", "other"], "countHist": [70, 66, 38, 17, 26]}, "datatypes": {}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": true}, {"path": "<http://schema.org/version>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 142, "uniqueEntitiesLanguages": 0, "percentValues": 100, "percentDatatypes": 100, "percentLanguages": 0, "totalValues": 142, "totalDatatypes": 142, "totalLanguages": 0, "uniqueValues": 142, "uniqueDatatypes": 1, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [], "summaryDatatypes": [{"value": "http://www.w3.org/2001/XMLSchema#integer", "count": 142}], "summaryLanguages": [], "summaryDistribution": {"quartiles": [882997342.75, 895965336.0, 896096200.5], "min": 863349056.125, "max": 915744487.125, "start": 846734804.0, "end": 1080964512.0, "outliers": [973448699, 965779776, 971393688, 958944307, 959967060, 924532756, 1076206026, 1076206039, 854042478, 915968916, 846734804, 987915894, 925325797, 925334193, 1044858143, 925315696, 925311519, 925316978, 925296130, 925295342, 925319506, 925300576, 925337135, 925320483, 925297976, 925298896, 925317541, 925298505, 925312552, 925321789, 925297292, 925299881, 923815997, 1080964512, 915972393, 915977051]}, "compareDistrib": {"values": {"countHist": [2, 52, 57, 21, 2, 3, 1, 0, 1, 3], "limitsHist": [846734804.0, 870157774.8, 893580745.6, 917003716.4, 940426687.2, 963849658.0, 987272628.8, 1010695599.6, 1034118570.4, 1057541541.2, 1080964512.0]}, "datatypes": {"limitsHist": ["http://www.w3.org/2001/XMLSchema#integer"], "countHist": [142]}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": true}, {"path": "<http://schema.org/dateModified>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 142, "uniqueEntitiesLanguages": 0, "percentValues": 100, "percentDatatypes": 100, "percentLanguages": 0, "totalValues": 142, "totalDatatypes": 142, "totalLanguages": 0, "uniqueValues": 128, "uniqueDatatypes": 1, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [{"value": "other", "count": 142}], "summaryDatatypes": [{"value": "http://www.w3.org/2001/XMLSchema#dateTime", "count": 142}], "summaryLanguages": [], "summaryDistribution": {}, "compareDistrib": {"values": {"limitsHist": ["other"], "countHist": [142]}, "datatypes": {"limitsHist": ["http://www.w3.org/2001/XMLSchema#dateTime"], "countHist": [142]}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": false}, {"path": "<http://www.w3.org/2000/01/rdf-schema#label>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 0, "uniqueEntitiesLanguages": 142, "percentValues": 100, "percentDatatypes": 0, "percentLanguages": 100, "totalValues": 151, "totalDatatypes": 0, "totalLanguages": 151, "uniqueValues": 143, "uniqueDatatypes": 0, "uniqueLanguages": 4, "countTotalEntities": 142, "summaryValues": [{"value": "other", "count": 151}], "summaryDatatypes": [], "summaryLanguages": [{"value": "en", "count": 142}, {"value": "other", "count": 9}], "summaryDistribution": {}, "compareDistrib": {"values": {"limitsHist": ["other"], "countHist": [151]}, "datatypes": {}, "languages": {"limitsHist": ["en", "other"], "countHist": [142, 9]}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": false}, {"path": "<http://wikiba.se/ontology#statements>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 142, "uniqueEntitiesLanguages": 0, "percentValues": 100, "percentDatatypes": 100, "percentLanguages": 0, "totalValues": 142, "totalDatatypes": 142, "totalLanguages": 0, "uniqueValues": 18, "uniqueDatatypes": 1, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [], "summaryDatatypes": [{"value": "http://www.w3.org/2001/XMLSchema#integer", "count": 142}], "summaryLanguages": [], "summaryDistribution": {"quartiles": [11.0, 12.0, 17.0], "min": 4.0, "max": 26.0, "start": 4.0, "end": 78.0, "outliers": [78]}, "compareDistrib": {"values": {"countHist": [46, 75, 20, 0, 0, 0, 0, 0, 0, 1], "limitsHist": [4.0, 11.4, 18.8, 26.200000000000003, 33.6, 41.0, 48.400000000000006, 55.800000000000004, 63.2, 70.60000000000001, 78.0]}, "datatypes": {"limitsHist": ["http://www.w3.org/2001/XMLSchema#integer"], "countHist": [142]}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": true}, {"path": "<http://wikiba.se/ontology#sitelinks>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 142, "uniqueEntitiesLanguages": 0, "percentValues": 100, "percentDatatypes": 100, "percentLanguages": 0, "totalValues": 142, "totalDatatypes": 142, "totalLanguages": 0, "uniqueValues": 2, "uniqueDatatypes": 1, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [], "summaryDatatypes": [{"value": "http://www.w3.org/2001/XMLSchema#integer", "count": 142}], "summaryLanguages": [], "summaryDistribution": {"quartiles": [0.0, 0.0, 0.0], "min": 0.0, "max": 0.0, "start": 0.0, "end": 2.0, "outliers": [2]}, "compareDistrib": {"values": {"countHist": [141, 0, 0, 0, 0, 0, 0, 0, 0, 1], "limitsHist": [0.0, 0.2, 0.4, 0.6000000000000001, 0.8, 1.0, 1.2000000000000002, 1.4000000000000001, 1.6, 1.8, 2.0]}, "datatypes": {"limitsHist": ["http://www.w3.org/2001/XMLSchema#integer"], "countHist": [142]}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": true}, {"path": "<http://wikiba.se/ontology#identifiers>", "uniqueEntitiesValues": 142, "uniqueEntitiesDatatypes": 142, "uniqueEntitiesLanguages": 0, "percentValues": 100, "percentDatatypes": 100, "percentLanguages": 0, "totalValues": 142, "totalDatatypes": 142, "totalLanguages": 0, "uniqueValues": 3, "uniqueDatatypes": 1, "uniqueLanguages": 0, "countTotalEntities": 142, "summaryValues": [], "summaryDatatypes": [{"value": "http://www.w3.org/2001/XMLSchema#integer", "count": 142}], "summaryLanguages": [], "summaryDistribution": {"quartiles": [0.0, 1.0, 1.0], "min": 0.0, "max": 2.0, "start": 0.0, "end": 2.0, "outliers": []}, "compareDistrib": {"values": {"countHist": [68, 0, 0, 0, 0, 72, 0, 0, 0, 2], "limitsHist": [0.0, 0.2, 0.4, 0.6000000000000001, 0.8, 1.0, 1.2000000000000002, 1.4000000000000001, 1.6, 1.8, 2.0]}, "datatypes": {"limitsHist": ["http://www.w3.org/2001/XMLSchema#integer"], "countHist": [142]}, "languages": {}}, "ks2": {"values": {}, "datatypes": {}, "languages": {}}, "reduction": false, "color": true},


selectedentities = [
    {
        "uri": "http://www.wikidata.org/entity/Q18205300", 
        "paths": [
            [["1962-04-02T00:00:00Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8", None, None]], 
            [["http://www.wikidata.org/entity/Q6581072", None, None]], 
            [["http://www.wikidata.org/entity/Q5", None, None]], 
            [["http://www.wikidata.org/entity/Q34", None, None]], 
            [["1103396032", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2020-01-24T02:50:10Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Ann-Sofi Sid\u00e9n", None, "sv"], ["Ann-Sofi Sid\u00e9n", None, "sw"], ["Ann-Sofi Sid\u00e9n", None, "it"]]
        ],
        "vectors": [512.2032761859894, 458.33973556518555]
    },
    {
        "uri": "http://www.wikidata.org/entity/Q58177677", 
        "paths": [
            [["1962-04-02T00:00:00Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8", None, None]], 
            [["http://www.wikidata.org/entity/Q6581072", None, None]], 
            [["http://www.wikidata.org/entity/Q5", None, None]], 
            [["http://www.wikidata.org/entity/Q34", None, None]], 
            [["1103396032", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2020-01-24T02:50:10Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            None
        ],
        "vectors": [775.7917554283142, 500.6682241821289]
    },
    {
        "uri": "http://www.wikidata.org/entity/Q737036", 
        "paths": [
            [["1962-04-02T00:00:00Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8", None, None]], 
            [["http://www.wikidata.org/entity/Q6581072", None, None]], 
            [["http://www.wikidata.org/entity/Q5", None, None]], 
             None, 
            [["1103396032", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2020-01-24T02:50:10Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Gunilla Bandolin", None, "sl"], ["Gunilla Bandolin", None, "sv"], ["Gunilla Bandolin", None, "sw"], ["Gunilla Bandolin", None, "it"], ["Gunilla Bandolin", None, "ga"]]
        ],
        "vectors": [769.416475496292, 374.14261234283447]
    },
    {
        "uri": "http://www.wikidata.org/entity/Q58291909", 
        "paths": [
            [["1962-04-02T00:00:00Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8", None, None]], 
            [["http://www.wikidata.org/entity/Q6581072", None, None]], 
            [["http://www.wikidata.org/entity/Q7", None, None]], 
            None, 
            [["1103396032", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2020-01-24T02:50:10Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Lenke Rothman", None, "sw"], ["Lenke Rothman", None, "sv"], ["Rothman Lenke", None, "sl"], ["Lenke Rothman", None, "it"]]
        ],
        "vectors": [134.86143619537353, 368.84218978881836]
    }
]



selectedentities11 = [
    {
        "uri": "http://www.wikidata.org/entity/Q58177677", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q58177677-F01A60E1-1367-4A56-91F0-1243AE70AB60", None, None]], 
            [["http://www.wikidata.org/entity/Q13442814", None, None]], 
            [["973448699", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-07-03T09:40:46Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["The use of Air Photos for Landform Maps", None, "en"], ["The use of Air Photos for Landform Maps", None, "nl"]], 
            [["30", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["wetenschappelijk artikel", None, "nl"], ["\u043d\u0430\u0443\u043a\u043e\u0432\u0430 \u0441\u0442\u0430\u0442\u0442\u044f, \u043e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u0430 \u0432 \u0433\u0440\u0443\u0434\u043d\u0456 1951", None, "uk"]]
        ], 
        "vectors": [49.70366401672363, 139.71313560009003]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q58291909", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q58291909-01B25B0B-54BB-4B29-8831-DC941BE94D58", None, None]], 
            [["http://www.wikidata.org/entity/Q13442814", None, None]], 
            [["965779776", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-06-20T19:35:00Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["James Warren Bagley, 1881\u20131947", None, "en"], ["James Warren Bagley, 1881\u20131947", None, "nl"]], 
            [["17", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["\u043d\u0430\u0443\u043a\u043e\u0432\u0430 \u0441\u0442\u0430\u0442\u0442\u044f, \u043e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u0430 \u0432 \u0447\u0435\u0440\u0432\u043d\u0456 1947", None, "uk"], ["wetenschappelijk artikel", None, "nl"]]
        ], 
        
        "vectors": [91.73205213546753, 135.43210310935973]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q58426937", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q58349780-D9C079FC-BF97-44E1-B04D-E671E5E4BA00", None, None]], 
            [["http://www.wikidata.org/entity/Q13442814", None, None]], [["971393688", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-06-29T22:53:44Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["The cartophile society of New England", None, "en"], ["The cartophile society of New England", None, "nl"]], 
            [["19", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["\u043d\u0430\u0443\u043a\u043e\u0432\u0430 \u0441\u0442\u0430\u0442\u0442\u044f, \u043e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u0430 \u0432 \u0441\u0456\u0447\u043d\u0456 1951", None, "uk"], ["article", None, "en"], ["wetenschappelijk artikel", None, "nl"]]
        ], 
        "vectors": [80.2043734550476, 129.63474819660186]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q58428041", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q58426937-42C8F1EE-2DDF-4BDE-91E0-623A65BD45D0", None, None]], 
            [["http://www.wikidata.org/entity/Q13442814", None, None]], 
            [["958944307", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-06-10T04:37:50Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Time charts of historical cartography", None, "en"], ["Time charts of historical cartography", None, "nl"]], 
            [["11", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["\u043d\u0430\u0443\u043a\u043e\u0432\u0430 \u0441\u0442\u0430\u0442\u0442\u044f, \u043e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u0430 \u0432 \u0441\u0456\u0447\u043d\u0456 1937", None, "uk"], ["wetenschappelijk artikel", None, "nl"]]
        ], 
        "vectors": [69.45965051651001, 139.64107961654662]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q60347105", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q58428041-E4C55FD2-DBAD-433A-AB10-7194288FA08A", None, None]], 
            [["http://www.wikidata.org/entity/Q13442814", None, None]], 
            [["959967060", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-06-11T10:45:05Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Colonel Stefan Lutsch von Luchsenstein 1710\u20131792", None, "en"], 
            ["Colonel Stefan Lutsch von Luchsenstein 1710\u20131792", None, "nl"]], 
            [["7", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["wetenschappelijk artikel", None, "nl"], ["\u043d\u0430\u0443\u043a\u043e\u0432\u0430 \u0441\u0442\u0430\u0442\u0442\u044f, \u043e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u0430 \u0432 \u0441\u0456\u0447\u043d\u0456 1953", None, "uk"]]
        ], 
        "vectors": [81.50759477615357, 127.56273658275603]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q60390698", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q60347105-bdd5830e-4d1e-993f-ea93-5549df156575", None, None], 
            ["http://www.wikidata.org/entity/statement/Q60347105-d67bd5e1-4786-63e7-dba7-b41f7214819a", None, None]], 
            [["http://www.wikidata.org/entity/Q42939539", None, None], ["http://www.wikidata.org/entity/Q47461344", None, None]], 
            [["896040506", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-03-28T18:11:47Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Erwin Raisz notebooks", None, "en"]], 
            [["78", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["travel diaries, cartographic materials, teaching materials, drafts of letters and speeches, and miscellaneous drawings by Erwin Raisz", None, "en"]]
        ], 
        "vectors": [103.99473581314086, 91.06771954298019]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q61209258", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q60390698-a59f5861-4b1b-265a-dff9-52bb8b1984e1", None, None], ["http://www.wikidata.org/entity/statement/Q60390698-a70de65a-4aa5-8f00-c2af-655183d80f4c", None, None]], 
            [["http://www.wikidata.org/entity/Q87167", None, None], ["http://www.wikidata.org/entity/Q47461344", None, None]], 
            [["924532756", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-04-25T20:02:11Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Notebook 9 [Erwin Raisz notebooks]", None, "en"]], 
            [["9", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["Notebook labeled 28 Erwin J. Raisz Department of Geology Columbia University New York", None, "en"]]
        ], 
        "vectors": [126.51688089370727, 136.3954535484314]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q61198222", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q61197687-1203D990-EB72-495A-ABEA-D50B8687F43A", None, None], ["http://www.wikidata.org/entity/statement/Q61197687-2dd0d383-4fc7-448a-07e9-3f5021f7150b", None, None], ["http://www.wikidata.org/entity/statement/Q61197687-7934f5c6-49e7-5d12-9123-fb6e23ce2a12", None, None]], 
            [["http://www.wikidata.org/entity/Q83790", None, None], ["http://www.wikidata.org/entity/Q7725634", None, None], ["http://www.wikidata.org/entity/Q47461344", None, None]], 
            [["1076206026", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-12-14T21:23:40Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Principles of Cartography", None, "en"]], 
            [["19", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["cartography manual", None, "en"]]
        ], 
        "vectors": [90.92552061080933, 144.51230585575104]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q62564071", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q61198222-c54de252-4863-aa50-9bf3-11002399322c", None, None], ["http://www.wikidata.org/entity/statement/Q61198222-d4c93f85-46d8-cf15-0c5e-52966c25ea34", None, None], ["http://www.wikidata.org/entity/statement/Q61198222-da6ceddc-4b9b-e3b6-7c2a-8f864bb18b31", None, None], ["http://www.wikidata.org/entity/statement/Q61198222-E3E75667-5BD0-452F-8366-E774104F4F72", None, None]], 
            [["http://www.wikidata.org/entity/Q162827", None, None], ["http://www.wikidata.org/entity/Q7725634", None, None], ["http://www.wikidata.org/entity/Q47461344", None, None], ["http://www.wikidata.org/entity/Q13136", None, None]], 
            [["1076206039", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-12-14T21:23:41Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["Atlas of Global Geography", None, "en"]], 
            [["15", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["1", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["atlas including \"trans-orbal\" map", None, "en"]]
        ], 
        "vectors": [72.98678741455078, 140.24880535602568]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q61209258", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q61209258-9a3224da-4ab8-10d7-00aa-905b40ea11ff", None, None], ["http://www.wikidata.org/entity/statement/Q61209258-e6aa3fbc-4fd4-79ae-acad-bce40dd7b549", None, None], ["http://www.wikidata.org/entity/statement/Q61209258-f9fe9217-4d8e-2896-56a8-e1a5bc3b072b", None, None]], 
            [["http://www.wikidata.org/entity/Q83790", None, None], ["http://www.wikidata.org/entity/Q47461344", None, None], ["http://www.wikidata.org/entity/Q13136", None, None]], 
            [["896090520", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-03-28T19:18:32Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["General cartography", None, "en"]], 
            [["24", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["textbook", None, "en"]]
        ], 
        "vectors": [76.37346858978272, 127.86023099422454]
    }, 
    {
        "uri": "http://www.wikidata.org/entity/Q62564075", 
        "paths": [
            [["http://www.wikidata.org/entity/statement/Q61209258-9a3224da-4ab8-10d7-00aa-905b40ea11ff", None, None], ["http://www.wikidata.org/entity/statement/Q61209258-e6aa3fbc-4fd4-79ae-acad-bce40dd7b549", None, None], ["http://www.wikidata.org/entity/statement/Q61209258-f9fe9217-4d8e-2896-56a8-e1a5bc3b072b", None, None]], 
            [["http://www.wikidata.org/entity/Q83790", None, None], ["http://www.wikidata.org/entity/Q47461344", None, None], ["http://www.wikidata.org/entity/Q13136", None, None]], 
            [["896090520", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2019-03-28T19:18:32Z", "http://www.w3.org/2001/XMLSchema#dateTime", None]], 
            [["General cartography", None, "en"]], 
            [["22", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["0", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["2", "http://www.w3.org/2001/XMLSchema#integer", None]], 
            [["textbook", None, "en"]]
        ], 
        "vectors": [76.37346858978272, 127.86023099422454]
    }
]

summarisedpaths = [
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'datecategory', 'summary': [{'count': 4, 'value': '1962-04-02T00:00:00Z'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://www.wikidata.org/prop/P31>',
        'percentDatatypes': 100,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'datecategory', 'summary': [{'count': 4, 'value': '1962-04-02T00:00:00Z'}]},
        'totalDatatypes': 4,
        'totalLanguages': 0,
        'totalValues': 4,
        'uniqueDatatypes': 1,
        'uniqueEntitiesDatatypes': 4,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 4,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://www.wikidata.org/prop/direct/P31>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 4,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 4,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/Q6581072'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://schema.org/version>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/Q6581072'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 4,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 4,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 3, 'value': 'http://www.wikidata.org/entity/Q5'}, {'count': 1, 'value': 'http://www.wikidata.org/entity/Q7'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://schema.org/dateModified>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 3, 'value': 'http://www.wikidata.org/entity/Q5'}, {'count': 1, 'value': 'http://www.wikidata.org/entity/Q7'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 4,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 4,
        'uniqueLanguages': 0,
        'uniqueValues': 2
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 2, 'value': 'http://www.wikidata.org/entity/Q34'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://www.w3.org/2000/01/rdf-schema#label>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 50,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 2, 'value': 'http://www.wikidata.org/entity/Q34'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 2,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 2,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': [{'count': 4,  'value': 'http://www.w3.org/2001/XMLSchema#integer'}]},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'numbercategory', 'summary': [{'count': 4, 'value': '1103396032', 'index': 1103396032}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://wikiba.se/ontology#statements>',
        'percentDatatypes': 100,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': [{'count': 4,  'value': 'http://www.w3.org/2001/XMLSchema#integer'}]},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'numbercategory', 'summary': [{'count': 4, 'value': '1103396032', 'index': 1103396032}]},
        'totalDatatypes': 4,
        'totalLanguages': 0,
        'totalValues': 4,
        'uniqueDatatypes': 1,
        'uniqueEntitiesDatatypes': 4,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 4,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'datecategory', 'summary': [{'count': 4, 'value': '2020-01-24T02:50:10Z'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://wikiba.se/ontology#sitelinks>',
        'percentDatatypes': 100,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'datecategory', 'summary': [{'count': 4, 'value': '2020-01-24T02:50:10Z'}]},
        'totalDatatypes': 4,
        'totalLanguages': 0,
        'totalValues': 4,
        'uniqueDatatypes': 1,
        'uniqueEntitiesDatatypes': 4,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 4,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': [{'count': 3, 'value': 'sw'}, {'count': 3, 'value': 'sv'}, {'count': 3, 'value': 'it'}, {'count': 2, 'value': 'sl'}, {'count': 1, 'value': 'ga'}]},
            'values': {'level': 'category', 'summary': [{'count': 5, 'value': 'Gunilla Bandolin'}, {'count': 3, 'value': 'Lenke Rothman'}, {'count': 3, 'value': 'Ann-Sofi Sidén'}, {'count': 1, 'value': 'Rothman Lenke'}]}
        },
        'countTotalEntities': 4,
        'ks2': {
            'datatypes': {},
            'languages': {},
            'values': {}
        },
        'path': '<http://wikiba.se/ontology#identifiers>',
        'percentDatatypes': 0,
        'percentLanguages': 75,
        'percentValues': 75,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': [{'count': 3, 'value': 'sw'}, {'count': 3, 'value': 'sv'}, {'count': 3, 'value': 'it'}, {'count': 2, 'value': 'sl'}, {'count': 1, 'value': 'ga'}]},
        'summaryValues': {'level': 'category', 'summary': [{'count': 5, 'value': 'Gunilla Bandolin'}, {'count': 3, 'value': 'Lenke Rothman'}, {'count': 3, 'value': 'Ann-Sofi Sidén'}, {'count': 1, 'value': 'Rothman Lenke'}]},
        'totalDatatypes': 0,
        'totalLanguages': 12,
        'totalValues': 12,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 3,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 5,
        'uniqueValues': 4
    }
]


comparedpaths = [
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'datecategory', 'summary': [{'count': 4, 'value': '1962-04-02T00:00:00Z'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://www.wikidata.org/prop/P31>',
        'percentDatatypes': 100,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': [{'count': 3, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'datecategory', 'summary': [{'count': 3, 'value': '1962-04-02T00:00:00Z'}]},
        'totalDatatypes': 3,
        'totalLanguages': 0,
        'totalValues': 3,
        'uniqueDatatypes': 1,
        'uniqueEntitiesDatatypes': 3,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://www.wikidata.org/prop/direct/P31>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/statement/q737036-A534E007-510B-4A9E-98AE-0B87D40DDDC8'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 3,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.wikidata.org/entity/Q6581072'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://schema.org/version>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 3, 'value': 'http://www.wikidata.org/entity/Q6581072'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 3,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 3, 'value': 'http://www.wikidata.org/entity/Q5'}, {'count': 1, 'value': 'http://www.wikidata.org/entity/Q7'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://schema.org/dateModified>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues':  {'level': 'category', 'summary': [{'count': 2, 'value': 'http://www.wikidata.org/entity/Q5'}, {'count': 1, 'value': 'http://www.wikidata.org/entity/Q7'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 3,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 0,
        'uniqueValues': 2
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'category', 'summary': [{'count': 2, 'value': 'http://www.wikidata.org/entity/Q34'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://www.w3.org/2000/01/rdf-schema#label>',
        'percentDatatypes': 0,
        'percentLanguages': 0,
        'percentValues': 33,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'category', 'summary': [{'count': 2, 'value': 'http://www.wikidata.org/entity/Q34'}]},
        'totalDatatypes': 0,
        'totalLanguages': 0,
        'totalValues': 1,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 1,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': [{'count': 4,  'value': 'http://www.w3.org/2001/XMLSchema#integer'}]},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'numbercategory', 'summary': [{'count': 4, 'value': '1103396032', 'index': 1103396032}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://wikiba.se/ontology#statements>',
        'percentDatatypes': 100,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': [{'count': 3,  'value': 'http://www.w3.org/2001/XMLSchema#integer'}]},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'numbercategory', 'summary': [{'count': 3, 'value': '1103396032', 'index': 1103396032}]},
        'totalDatatypes': 3,
        'totalLanguages': 0,
        'totalValues': 3,
        'uniqueDatatypes': 1,
        'uniqueEntitiesDatatypes': 3,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': [{'count': 4, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
            'languages': {'level': 'category', 'summary': []},
            'values': {'level': 'datecategory', 'summary': [{'count': 4, 'value': '2020-01-24T02:50:10Z'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://wikiba.se/ontology#sitelinks>',
        'percentDatatypes': 100,
        'percentLanguages': 0,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': [{'count': 3, 'value': 'http://www.w3.org/2001/XMLSchema#dateTime'}]},
        'summaryLanguages': {'level': 'category', 'summary': []},
        'summaryValues': {'level': 'datecategory', 'summary': [{'count': 3, 'value': '2020-01-24T02:50:10Z'}]},
        'totalDatatypes': 3,
        'totalLanguages': 0,
        'totalValues': 3,
        'uniqueDatatypes': 1,
        'uniqueEntitiesDatatypes': 3,
        'uniqueEntitiesLanguages': 0,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 0,
        'uniqueValues': 1
    },
    {
        'compareDistrib': {
            'datatypes': {'level': 'category', 'summary': []},
            'languages': {'level': 'category', 'summary': [{'count': 3, 'value': 'sw'}, {'count': 3, 'value': 'sv'}, {'count': 3, 'value': 'it'}, {'count': 2, 'value': 'sl'}, {'count': 1, 'value': 'ga'}]},
            'values': {'level': 'category', 'summary': [{'count': 5, 'value': 'Gunilla Bandolin'}, {'count': 3, 'value': 'Lenke Rothman'}, {'count': 3, 'value': 'Ann-Sofi Sidén'}, {'count': 1, 'value': 'Rothman Lenke'}]}
        },
        'countTotalEntities': 3,
        'ks2': {},
        'path': '<http://wikiba.se/ontology#identifiers>',
        'percentDatatypes': 0,
        'percentLanguages': 100,
        'percentValues': 100,
        'summaryDatatypes': {'level': 'category', 'summary': []},
        'summaryLanguages': {'level': 'category', 'summary': [{'count': 3, 'value': 'sw'}, {'count': 3, 'value': 'sv'}, {'count': 3, 'value': 'it'}, {'count': 2, 'value': 'sl'}, {'count': 1, 'value': 'ga'}]},
        'summaryValues': {'level': 'category', 'summary': [{'count': 5, 'value': 'Gunilla Bandolin'}, {'count': 3, 'value': 'Lenke Rothman'}, {'count': 3, 'value': 'Ann-Sofi Sidén'}, {'count': 1, 'value': 'Rothman Lenke'}]},
        'totalDatatypes': 0,
        'totalLanguages': 12,
        'totalValues': 12,
        'uniqueDatatypes': 0,
        'uniqueEntitiesDatatypes': 0,
        'uniqueEntitiesLanguages': 3,
        'uniqueEntitiesValues': 3,
        'uniqueLanguages': 5,
        'uniqueValues': 4
    }
]