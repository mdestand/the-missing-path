import numpy as np
import pandas as pd
import os
import bz2
import json

class Popularity:
    def __init__(self, entities):
        self.entities = entities
        self.names = self.getNames()
        self.ranks = []

    def extract(self):
        self.ranks = self.resolve('en.z', self.names)
        def getEntityWithPopularity(entity):
            title = entity['uri'].replace('http://dbpedia.org/resource/', '')
            entity['popularity'] = float(self.ranks[title]) if self.ranks[title] else 0
            return entity
            
        self.entities = list(map(lambda entity:getEntityWithPopularity(entity), self.entities))
        return self.entities
        #return []


    def getNames(self):
        return [entity['uri'].replace('http://dbpedia.org/resource/', '') for entity in self.entities]

    def resolve(self, project_code, titles):
        articles_scores = pd.Series(1, index=titles)
        # Get file at https://dumps.wikimedia.org/other/pagecounts-ez/merged/pagecounts-2019-09-views-ge-5-totals.bz2
        filename = 'files/dump/pagecounts-2019-09-views-ge-5-totals.bz2'
        length = os.path.getsize(filename)
        with bz2.open(filename, "rb") as ifile:
            iter_csv = pd.read_csv(ifile, sep=' ',
                                iterator=True, chunksize=100000,
                                header=None,
                                names=['project', 'title', 'count'])
            for chunk in iter_csv:
                print('%s%%'%( int(1000*ifile._fp.tell()/length)/10))
                touched = False
                project_entries = chunk[chunk['project'] == project_code]
                for entry in project_entries.itertuples(index=None, name=None):
                    title = entry[1]
                    if isinstance(title, str):
                        #title = title.replace('_', ' ')
                        if title in articles_scores:
                            articles_scores[title] += entry[2]
                            touched = True
                if touched and (articles_scores > 1).all():
                    break

        return articles_scores
