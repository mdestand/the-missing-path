from Path import Path
import numpy as np
import pandas as pd
import requests
from math import floor
from math import ceil
from datetime import datetime
from functools import reduce
from scipy import stats


class Summary:
    def __init__(self, paths, entities, selectedentities, selectedpaths, prefixes = {}):
        self.paths = paths
        self.entities = entities
        self.prefixes = prefixes
        self.selectedpaths = selectedpaths
        # uses the list of URIs in raw entities to get a filtered aray o
        self.selectedentities = list(filter(lambda ent:ent['uri'] in selectedentities, self.entities.copy())) if selectedentities is not None else self.entities.copy()
        self.checkdistribution = selectedentities is not None
        self.summarisedpaths = []
        self.missing = []
        #self.getSummary(minpercentsummary)


    def getSummary(self, minpercentsummary, start=0, stop=0):
        paths = self.paths.copy()
        if stop == 0:
            stop = len(paths) 
        for index in range(start, stop):
            path = paths[index]
            thepath = Path(path, index, len(self.entities), self.selectedentities.copy(), minpercentsummary, 30, self.checkdistribution, self.prefixes)
            self.summarisedpaths.append(thepath.dict)
        return self.summarisedpaths[start:stop]


    def getMissing(self):
        self.missing = []
        #print( self.selectedpaths)
        for pathindex, fpath in enumerate(self.paths):
            path = fpath['path']
            entitiespath = list(map(lambda entity:entity['paths'][pathindex], self.selectedentities))
            entitiesnull = list(filter(lambda value:value is None, entitiespath))
            #print(path, len(self.selectedentities), len(entitiesnull), len(self.selectedentities) == len(entitiesnull), path in self.selectedpaths)
            if len(self.selectedentities) == len(entitiesnull) and path in self.selectedpaths:
                self.missing.append(path)
        return self.missing
   

    def getValuesPath(self, selectedpath):
        for index, path in enumerate(self.paths):
            if selectedpath == path['path']:
                pathindex = index
        selectedentities = self.selectedentities.copy()
        entitiespath = list(map(lambda entity:entity['paths'][pathindex], selectedentities))
        entitiesnotnull = list(filter(lambda value:value is not None, entitiespath))
        if len(entitiesnotnull) > 0:
            def flatten(a,b):
                a.extend(b)
                return a
            flatentities = reduce(flatten, entitiesnotnull)
            minvaluesummary = 1 if len(flatentities) < 35 else len(flatentities) * minpercentsummary / 100
            flatdatatypes = list(filter(lambda value:value[1] is not None, flatentities))
            flatlanguages = list(filter(lambda value:value[2] is not None, flatentities))
            minvaluesummarylang = 1 if len(flatlanguages) < 35 else len(flatlanguages) * minpercentsummary / 100
            values = pd.DataFrame(flatentities)
            return values[0].nunique()
        else:
            return []
