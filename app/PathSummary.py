import numpy as np
import pandas as pd
import os
import bz2
import json
from math import ceil, floor
from functools import reduce
from scipy import stats


class PathSummary:
    def __init__(self, summarytype, series, totalvalues, minvaluesperitem, maxitems, maintype, saved=None):
        self.summarytype = summarytype
        self.series = series
        self.totalvalues = totalvalues
        self.minvaluesperitem = minvaluesperitem
        self.maxitems = maxitems
        self.maintype = maintype
        self.saved = saved
        #self.valuecounts
        #self.summary
        #self.level
        #self.bins
        #self.counts
        #self.other
        self.ks2 = {}
        self.summarise()


    def summarise(self):
        if self.maintype == 'date':
            self.valuecounts = self.series.value_counts()
            if len(self.valuecounts) <= self.maxitems and not (self.saved is not None and  self.saved["level"] in ["years", "months", "days", "hours"]):
                self.level = 'datecategory'
                self.valuecounts = self.valuecounts.items()
            else: 
                self.dates = pd.DataFrame(pd.to_datetime(self.series, errors='coerce').dropna())
                self.dates["years"] = self.dates[0]
                self.dates["months"] = self.dates[0]
                self.dates["days"] = self.dates[0]
                self.dates["hours"] = self.dates[0]
                self.dates = self.dates.set_index("hours")
                self.hours = self.dates.to_period(freq='H').index.value_counts()
                self.dates = self.dates.set_index("days")
                self.days = self.dates.to_period(freq='D').index.value_counts()
                self.dates = self.dates.set_index("months")
                self.months = self.dates.to_period(freq='M').index.value_counts()
                self.dates = self.dates.set_index("years")
                self.years = self.dates.to_period(freq='Y').index.value_counts()

                self.valuecounts = []
                #print(len(years), len(months), len(days), len(hours))
                if len(self.years) > 1:
                    for period, count in self.years.items():
                        self.valuecounts.append((str(period), count))
                    self.level = 'years'
                elif len(self.months) > 1: 
                    for period, count in self.months.items():
                        self.valuecounts.append((str(period), count))
                    self.level = 'months'
                elif len(self.days) > 1: 
                    for period, count in self.days.items():
                        self.valuecounts.append((str(period), count))
                    self.level = 'days'
                else:
                    for period, count in self.hours.items():
                        self.valuecounts.append((str(period), count))
                    self.level = 'hours'
        elif self.maintype == 'number':
            self.valuecounts = self.series.value_counts()
            if len(self.valuecounts) <= self.maxitems  and not(self.saved is not None and self.saved["level"] == 'number'):
                self.level = 'numbercategory'
                self.valuecounts = self.valuecounts.items()
            else: 
                self.numbers = pd.DataFrame(pd.to_numeric(self.series, errors='coerce').dropna())
                #print(self.numbers)
                self.valuesmax = self.numbers[0].max()
                self.valuesmin = self.numbers[0].min()
                self.valuespowermin = 10**(int(len(str(floor(self.valuesmin))))-1)
                self.valuesstart = 0 if self.valuespowermin == 1 else self.valuespowermin
                self.valuesrange = self.valuesmax - self.valuesstart
                self.factor = 10 * self.valuespowermin
                self.times = ceil(self.valuesrange / self.factor)
                while self.times > 19:
                    self.factor += 10 * self.valuespowermin
                    self.times = ceil(self.valuesrange / self.factor)                
                self.valuesend = self.valuesstart + (self.times * self.factor)
                interval_range = pd.interval_range(start=self.valuesstart, freq=self.factor, end=self.valuesend)
                #print(interval_range)
                self.valuecounts = self.numbers[0].value_counts(bins=interval_range, sort=False).items()
                self.level = 'number'
        elif self.maintype == 'category':
            self.valuecounts = self.series.value_counts().items()
            self.level = 'category'

        self.summary = self.formatSum(self.valuecounts, self.level)

        
    def formatSum(self, valuecounts, level):
         # format summary
        summary = []
        total = 0
        
        for key, val in valuecounts:
            if (level == 'category' and val >= self.minvaluesperitem) or (level != 'category' and val != 0):
                total = total + val
                if level == 'number':
                    index =  float(key.left)
                    newkey = str([key.left, key.right]) 
                    summary.append({'value': newkey,'count': int(val), 'index': int(index) })
                elif level == 'numbercategory':
                    summary.append({'value': str(key),'count': int(val), 'index': float(key) })
                else:                  
                    summary.append({'value': str(key),'count': int(val) })
        if level == 'category':
            summary.sort(reverse = True, key=lambda x: (x['count'], x['value']))
        elif level == 'number' or level == 'numbercategory':
            summary.sort(reverse = False, key=lambda x: (x['index'], x['value'], x['count']))
        else:
            summary.sort(reverse = False, key=lambda x: (x['value'], x['count']))
            
        if total < self.totalvalues:
            summary.append({'value': 'other','count': int(self.totalvalues - total)})
        return summary                

    def compare(self):
        if len(self.saved["summary"]) > 0 and len(self.summary) > 0:
            oldcount = []
            newcount = []
            dictionnary = {}
            if self.saved["level"] == 'number':
                oldbins = []
                for element in self.saved["summary"] :
                    oldcount.append(element['count'])
                    newcount.append(0)
                    if element['value'] != 'other':
                        splitted = element['value'].replace('[', '').replace(']', '').split(',')
                        oldbins.append([splitted[0], splitted[1]])
                for index, element in enumerate(self.valuecounts):
                    for curbin in oldbins:
                        if element.value >= curbin[0] and element["value"] <= curbin[1]:
                            newcount[index] =  newcount[index] + element["count"]

            else: #category or date
                if self.saved["level"] == 'category' or self.saved["level"] == 'numbercategory' or self.saved["level"] == 'datecategory':
                    for element in self.summary:
                        dictionnary[element['value']] = element['count']
                else:
                    #date
                    newsummary = []
                    refdate = self.hours if self.saved["level"] == "hours" else self.days if self.saved["level"] == "days" else self.months if self.saved["level"] == "months" else self.years
                    for period, count in refdate.items():
                        newsummary.append((period, count))
                    newvaluecounts = self.formatSum(newsummary, self.level)
                    for element in newvaluecounts:
                        dictionnary[element['value']] = element['count']

                for element in self.saved["summary"] :
                    oldcount.append(element["count"])
                    if element["value"] in dictionnary:
                        newcount.append(dictionnary[element["value"]])
                        del dictionnary[element["value"]]
                    else:
                        newcount.append(0)
                # add remaining to 'other' count
                if len(newcount) == 0:
                    newcount = [0]
                for key in dictionnary:                
                    newcount[len(newcount)-1] = newcount[len(newcount)-1] + dictionnary[key]
            self.newcount = newcount
            ks2coeff, ks2p = stats.ks_2samp(oldcount, newcount)
            self.ks2['coeff'] = ks2coeff
            self.ks2['p'] = ks2p
        return self.ks2
