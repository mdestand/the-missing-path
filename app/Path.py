from PathSummary import PathSummary
import numpy as np
import pandas as pd
import os
import bz2
import json
from math import ceil
from functools import reduce

numericaltypes = [
    "http://www.w3.org/2001/XMLSchema#integer",
    "http://www.w3.org/2001/XMLSchema#gYear",
    "http://www.w3.org/2001/XMLSchema#double",
    "number"
]
datetimetypes = [
    "http://www.w3.org/2001/XMLSchema#dateTime"
]

class Path:
    def __init__(self, path, indexpath, counttotalentities, selectedentities, minpercentsummary, maxitems, checkdistribution, prefixes = {}):
        self.path = path
        self.reduction = path["reduction"] if "reduction" in path else False
        self.color = path["color"] if "color" in path else False
        self.prefixed = path["prefixed"] if ("prefixed" in path and path["prefixed"] is not None) else self.prefix(path['path'], prefixes)
        self.uri = path["path"]
        self.index = indexpath
        self.counttotalentities = counttotalentities
        self.minpercentsummary = minpercentsummary
        self.checkdistribution = checkdistribution
        self.maxitems = maxitems
        self.selectedentities = list(map(lambda entity:entity['paths'][indexpath], selectedentities))
        self.coeff = len(selectedentities) * 100 / counttotalentities
        self.compareDistrib = {'datatypes': {'summary': [], 'level': '' }, 'languages':  {'summary': [], 'level': '' }, 'values':  {'summary': [], 'level': '' }}
        self.valuesnotnull = []
        self.datatypesnotnull = []
        self.languagesnotnull = []
        self.flatvalues = []
        self.entitiesdataframe = []
        self.countflatdataframe = [0,0,0]
        self.uniqueValues = []
        self.uniqueDatatypes = []
        self.uniqueLanguages = []
        self.minvaluesummarydatatypes = []
        self.summarydatatypes = {'summary': [], 'level': ''}
        self.minvaluesummarylanguages = []
        self.summarylanguages = {'summary': [], 'level': ''}
        self.minvaluesummary = []
        self.summaryvalues = {'summary': [], 'level': ''}
        #
        self.extract()
        #
        self.ks2 = {'datatypes': {}, 'languages': {}, 'values': {}}
        
        if self.checkdistribution:
            self.compareDistrib = {'datatypes': path["compareDistrib"]["datatypes"], 'languages': path["compareDistrib"]["languages"], 'values': path["compareDistrib"]["values"]}
            self.analyse()
            self.compare()
        else:
            self.analyse()
            if len(self.valuesnotnull)> 0:
                self.compareDistrib = {'datatypes':  {'summary': self.summarydatatypes.summary, 'level': self.summarydatatypes.level}, 'languages':  {'summary': self.summarylanguages.summary, 'level': self.summarylanguages.level}, 'values':  {'summary': self.summaryvalues.summary, 'level': self.summaryvalues.level}}
            self.save()
        self.dict = self.retrieve()

    def prefix(self, path, prefixes):
        prefixed = None
        nakedpath = path.replace('<','').replace('>','')
        for pref in prefixes:
            if nakedpath.find(prefixes[pref]) == 0 :
                prefixed = nakedpath.replace(prefixes[pref], pref+':')
                break;
        return prefixed 


    def extract(self): 
        self.valuesnotnull =  list(filter(lambda value:value is not None, self.selectedentities))
        if len(self.valuesnotnull)> 0:
            self.datatypesnotnull = list(filter(lambda value: len(list(filter(lambda v:v[1] is not None, value))) > 0 is not None, self.valuesnotnull))
            self.languagesnotnull = list(filter(lambda value: len(list(filter(lambda v:v[2] is not None, value))) > 0 is not None, self.valuesnotnull))

            def flatten(a,b):
                a.extend(b)
                return a
            self.flatvalues = reduce(flatten, self.valuesnotnull,[])
            self.entitiesdataframe = pd.DataFrame(self.flatvalues)
            self.countflatdataframe = self.entitiesdataframe.count()
            #print(self.entitiesdataframe[0])
            self.uniqueValues = self.entitiesdataframe[0].nunique()
            self.uniqueDatatypes = self.entitiesdataframe[1].nunique()
            self.uniqueLanguages = self.entitiesdataframe[2].nunique()
        

    def analyse(self):
        if len(self.valuesnotnull)> 0:
            self.minvaluesummarydatatypes = 1 if self.countflatdataframe[1] < self.maxitems else self.countflatdataframe[1] * self.minpercentsummary / 100
            saveddatatypes = self.compareDistrib['datatypes'] if self.checkdistribution else None
            self.summarydatatypes = PathSummary('datatypes', self.entitiesdataframe[1], self.countflatdataframe[1], self.minvaluesummarydatatypes, self.maxitems, 'category', saveddatatypes)
            #
            self.minvaluesummarylanguages = 1 if self.countflatdataframe[2] < self.maxitems else self.countflatdataframe[2] * self.minpercentsummary / 100
            savedlanguages = self.compareDistrib['languages'] if self.checkdistribution else None
            self.summarylanguages = PathSummary('languages', self.entitiesdataframe[2], self.countflatdataframe[2], self.minvaluesummarylanguages, self.maxitems, 'category', savedlanguages)
            #
            self.minvaluesummary = 1 if self.countflatdataframe[0] < self.maxitems else self.countflatdataframe[0] * self.minpercentsummary / 100
            maintype = 'category'
            if len(self.summarydatatypes.summary) > 0:
                if self.summarydatatypes.summary[0]['value'] in datetimetypes:
                    maintype = 'date'
                elif self.summarydatatypes.summary[0]['value'] in numericaltypes:
                    maintype = 'number'
            savedvalues = self.compareDistrib['values'] if self.checkdistribution else None
            self.summaryvalues = PathSummary('values', self.entitiesdataframe[0], self.countflatdataframe[0], self.minvaluesummary, self.maxitems, maintype, savedvalues)

    
    def save(self):
        if round(len(self.valuesnotnull) / self.counttotalentities * 100) >= 10:
            self.compareDistrib['datatypes'] = {'summary': self.summarydatatypes.summary, 'level': self.summarydatatypes.level}
            self.compareDistrib['languages'] = {'summary': self.summarylanguages.summary, 'level': self.summarylanguages.level}
            self.compareDistrib['values'] = {'summary': self.summaryvalues.summary, 'level': self.summaryvalues.level}

    def compare(self):
        if len(self.valuesnotnull)> 0:
            self.ks2['datatypes'] = self.summarydatatypes.compare()
            self.ks2['languages'] = self.summarylanguages.compare()
            self.ks2['values'] = self.summaryvalues.compare()


    def retrieve(self):
        return {
            'path': self.uri,
            'color': self.color,
            'reduction': self.reduction,
            'prefixed': self.prefixed,
            'uniqueEntitiesValues': len(self.valuesnotnull),
            'uniqueEntitiesDatatypes': len(self.datatypesnotnull),
            'uniqueEntitiesLanguages': len(self.languagesnotnull),
            'percentValues': round(len(self.valuesnotnull) / len(self.selectedentities) * 100),
            'percentDatatypes': round(len(self.datatypesnotnull) / len(self.selectedentities) * 100),
            'percentLanguages': round(len(self.languagesnotnull) / len(self.selectedentities) * 100),
            'totalValues': int(self.countflatdataframe[0]),
            'totalDatatypes': int(self.countflatdataframe[1]),
            'totalLanguages': int(self.countflatdataframe[2]),
            'uniqueValues': self.uniqueValues,
            'uniqueDatatypes': self.uniqueDatatypes,
            'uniqueLanguages': self.uniqueLanguages,
            'countTotalEntities': len(self.selectedentities),
            'summaryValues': {'summary': self.summaryvalues.summary, 'level': self.summaryvalues.level} if len(self.valuesnotnull) > 0 else self.summaryvalues,
            'summaryDatatypes': {'summary': self.summarydatatypes.summary, 'level': self.summarydatatypes.level} if len(self.valuesnotnull) > 0 else self.summarydatatypes,
            'summaryLanguages': {'summary': self.summarylanguages.summary, 'level': self.summarylanguages.level} if len(self.valuesnotnull) > 0 else self.summarylanguages,
            'compareDistrib': self.compareDistrib,
            'ks2': self.ks2,
        }