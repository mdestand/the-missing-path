module.exports = {
  rules: {
    "no-console": "off",
  },
  parserOptions: {
    sourceType: "module",
  },
  extends: ["plugin:vue/base"],
};
