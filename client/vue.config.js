module.exports = {
  devServer: {
    disableHostCheck: true,
    hot: true,
    public: "192.168.99.103",
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000,
    },
    headers: {
      "Access-Control-Allow-Origin": "192.168.99.103",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
  },
};
