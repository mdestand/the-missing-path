import Vue from "vue";
import Vuex from "vuex";
import * as d3 from "d3";
import { Delaunay } from "d3-delaunay";
//import vm from './main.js';
//import io from 'socket.io-client';
//import { path } from 'd3';

Vue.use(Vuex);

// root state object.
// each Vuex instance is just a single state tree.
const state = {
  activeURI: undefined,
  activeURIDetail: {},
  api: process.env.VUE_APP_PROTOCOL + "://" + process.env.VUE_APP_SERVER,
  apiws: process.env.VUE_APP_PROTOCOLWS + "://" + process.env.VUE_APP_SERVER,
  collection: "comics",
  collections: [
    "off_testbreakfastcereals",
    "nobellaureate",
    "comics",
    "patinage",
    "patinage2",
    "illuminatinew",
    "illuminatiQ28114",
    "illuminatiQ99677",
    "maps",
    "womenscientists",
  ],
  color: null,
  colorfunction: undefined,
  colorvisible: false,
  conditions: {},
  conditionslength: 0,
  constraintsvisible: undefined,
  currentkey: undefined,
  entities: [],
  entitiesvisible: false,
  exportconditions: {},
  gradientcolors: [],
  hoveredpath: undefined,
  hoveredmissing: [],
  hoveredzone: undefined,
  indexes: [],
  label: false,
  lasso: [],
  loading_settings: false,
  loading_projection: false,
  loading_selection: false,
  minclusters: undefined,
  openpath: undefined,
  openpaths: [],
  paths: [],
  pathsstate: 1,
  pathssubset: [],
  popularitythreshold: 1,
  prefixes: {
    factgrid: "https://database.factgrid.de/",
    wd: "http://www.wikidata.org/entity/",
    wds: "http://www.wikidata.org/entity/statement/",
    wdv: "http://www.wikidata.org/value/",
    wdt: "http://www.wikidata.org/prop/direct/",
    wikibase: "http://wikiba.se/ontology#",
    schema: "http://schema.org/",
    rdfs: "http://www.w3.org/2000/01/rdf-schema#",
    p: "http://www.wikidata.org/prop/",
    ps: "http://www.wikidata.org/prop/statement/",
    pq: "http://www.wikidata.org/prop/qualifier/",
    bd: "http://www.bigdata.com/rdf#",
    skos: "http://www.w3.org/2004/02/skos/core#",
  },
  preflanguages: ["fr"],
  projectionvisible: false,
  selection: [],
  socket: null,
  settingsvisible: false,
  sliderposition: 0,
  URIDetail: {},
  width: 1,
  widthmap: 1,
  widthmarginmap: 1,
  widthpathpattern: 1,
  widthset: 1,
  zones: [],
  zoom: [0, 0, 100],
};

// mutations are operations that actually mutate the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {
  addEntity(state, entity) {
    if (!state.selection.includes(entity)) state.selection.push(entity);
  },
  addOpenPath(state, index) {
    if (!state.openpaths.includes(index)) state.openpaths.push(index);
  },
  addToLasso(state, points) {
    if (state.hoveredzone) state.hoveredzone = undefined;
    state.lasso.push(points);
  },
  addLassoToConditions(state, selection) {
    state.conditions["lasso"] = selection;
  },
  addSelectionSummary(state, summary) {
    summary.forEach((newpath) => {
      let index = state.indexes[newpath.path];
      state.pathssubset[index] = newpath;
    });
  },
  addSummary(state, summary) {
    summary.forEach((newpath) => {
      let index = state.indexes[newpath.path];
      state.paths[index] = newpath;
    });
  },
  addURIDetail(state, params) {
    state.URIDetail[params.URI] = params.URIdesc;
    localStorage.setItem("missingURIDetail", JSON.stringify(state.URIDetail));
  },
  clearConditions(state) {
    state.conditions = {};
  },
  clearSelection(state) {
    state.selection = [];
  },
  countConditions(state) {
    state.conditionslength = Object.keys(state.conditions)
      .map((key) => (key === "lasso" ? "lasso" : state.conditions[key]))
      .reduce((acc, cur) => {
        if (cur === "lasso") {
          acc += 1;
        } else {
          Object.keys(cur).forEach((type) => {
            acc += Object.keys(cur[type]).length;
          });
        }
        return acc;
      }, 0);
  },
  nextPath() {
    state.openpath = !state.openpath ? 1 : state.openpath + 1;
  },
  nextPathState() {
    state.pathsstate = state.pathsstate < 2 ? state.pathsstate + 1 : 1;
  },
  previousPath() {
    state.openpath = !state.openpath ? 1 : state.openpath - 1;
  },
  removeCondition(state, params) {
    delete state.conditions[params.path][params.type][params.value];
    if (Object.keys(state.conditions[params.path][params.type]).length === 0)
      delete state.conditions[params.path][params.type];
    if (Object.keys(state.conditions[params.path]).length === 0)
      delete state.conditions[params.path];
  },
  removeEntity(state, entity) {
    state.selection = state.selection.filter((e) => e !== entity);
  },
  removeLassoFromConditions(state) {
    delete state.conditions["lasso"];
    state.lasso = [];
  },
  removeOpenPath(state, index) {
    state.openpaths = state.openpaths.filter((e) => e !== index);
  },
  resetLasso(state) {
    state.lasso = [];
  },
  setActiveURI(state, URI) {
    state.activeURI = URI;
    state.activeURIDetail = {};
  },
  setActiveURIDetail(state, details) {
    state.activeURIDetail = details;
  },
  setCoeff(state, coeff) {
    state.coeff = coeff;
  },
  setCollection(state, coll) {
    state.collection = coll;
    localStorage.setItem("missingCollection", coll);
  },
  setColor(state, colorIndex) {
    state.color = colorIndex;
  },
  setColorFunction(state, fn) {
    state.colorfunction = fn;
  },
  setColorVisible(state, isVisible) {
    state.colorvisible = isVisible;
    if (state.colorvisible) {
      state.settingsvisible = false;
      state.projectionvisible = false;
    }
  },
  setConstraintsVisible(state, isVisible) {
    state.constraintsvisible = isVisible;
  },
  setEntities(state, entities) {
    state.entities = entities;
  },
  setEntitiesLabels(state, entitieslabels) {
    let labels = {};
    entitieslabels.forEach((e) => {
      labels[e[0]] = e[3];
    });
    state.entitieslabels = labels;
  },
  setEntitiesVisible(state, isVisible) {
    state.entitiesvisible = isVisible;
  },
  setGradientColors(state, colors) {
    state.gradientcolors = colors;
  },
  setHoveredMissing(state, missing) {
    state.hoveredmissing = missing;
  },
  setHoveredPath(state, indexPath) {
    state.hoveredpath = indexPath;
    state.hoveredzone = undefined;
    state.hoveredmissing = [];
  },
  setHoveredZone(state, indexZone) {
    state.hoveredzone = indexZone;
  },
  setOpenPath(state, indexPath) {
    state.openpath = indexPath;
  },
  setPopularityThreshold(state, threshold) {
    state.popularitythreshold = threshold;
  },
  setPrefLanguages(state, lang) {
    state.preflanguages = lang;
    localStorage.setItem("preflanguages", JSON.stringify(lang));
  },
  setProjectionVisible(state, isVisible) {
    state.projectionvisible = isVisible;
    if (state.projectionvisible) {
      state.settingsvisible = false;
      state.colorvisible = false;
    }
  },
  setSelectionSummary(state, summary) {
    state.pathssubset = summary;
  },
  setSettingsVisible(state, isVisible) {
    state.settingsvisible = isVisible;
    if (state.settingsvisible) {
      state.projectionvisible = false;
      state.colorvisible = false;
    }
  },
  setSummary(state, summary) {
    state.paths = summary;
    state.indexes = {};
    state.paths.forEach((path, index) => {
      state.indexes[path.path] = index;
    });
  },
  setWidthType(state, params) {
    state["width" + params.type] = params.value;
  },
  setZoom(state, coords) {
    if (coords.length > 2) {
      state.zoom = coords;
    } else {
      state.zoom = [coords[0], coords[1], state.zoom[2]];
    }
  },
  setZones(state, zones) {
    state.zones = zones;
  },
  setSliderPosition(state, pos) {
    state.sliderposition = pos;
  },
  toggleColor(state) {
    state.colorvisible = !state.colorvisible;
    if (state.colorvisible) {
      state.settingsvisible = false;
      state.projectionvisible = false;
    }
  },
  toggleConditions(state) {
    state.constraintsvisible = !state.constraintsvisible;
    if (state.constraintsvisible) {
      state.entitiesvisible = false;
    }
  },
  toggleEntities(state) {
    state.entitiesvisible = !state.entitiesvisible;
    if (state.entitiesvisible) {
      state.constraintsvisible = false;
    }
  },
  toggleLabel(state) {
    state.label = !state.label;
  },
  toggleProjection(state) {
    state.projectionvisible = !state.projectionvisible;
    if (state.projectionvisible) {
      state.settingsvisible = false;
      state.colorvisible = false;
    }
  },
  toggleSettings(state) {
    state.settingsvisible = !state.settingsvisible;
    if (state.settingsvisible) {
      state.projectionvisible = false;
      state.colorvisible = false;
    }
  },
};

// actions are functions that cause side effects and can involve
// asynchronous operations.
const actions = {
  socket_connect() {},
  socket_summaryWs({ commit, state }, data) {
    //
    if (data) data = JSON.parse(data);
    if (data.key === state.currentkey) {
      if (data.key === "full") {
        commit("addSummary", data.summary);
      } else {
        commit("addSelectionSummary", data.summary);
      }
    }
  },
  socket_endSummaryWs({ state }, data) {
    if (data === state.currentkey) {
      state.loading_selection = false;
    }
  },
  addCondition({ state, commit }, params) {
    if (state.conditionslength < 5) {
      if (!state.conditions[params.path]) state.conditions[params.path] = {};
      if (!state.conditions[params.path][params.type])
        state.conditions[params.path][params.type] = {};
      state.conditions[params.path][params.type][params.value] = [
        params.value,
        params.inverse,
        params.subset,
      ];
      if (params.value === "other") {
        let path = params.subset
          ? state.pathssubset[params.index]
          : state.paths[params.index];
        let sum =
          params.type === "values" ? path.summaryValues : path.summaryLanguages;
        state.conditions[params.path][params.type][params.value].push(sum);
      } else {
        state.conditions[params.path][params.type][params.value].push(null);
      }
      commit("countConditions");
    } else {
      alert("A selection can combine a maximum of 5 conditions");
    }
  },
  addLassoSelectionToConditions({ state, commit }) {
    let lasso = zoomLasso(state.lasso, state.zoom, state.widthmarginmap);
    let selected = state.entities
      .filter((entity) => {
        return isInside([entity[1], entity[2]], lasso);
      })
      .map((entity) => entity[0]);
    commit("addLassoToConditions", selected);
    commit("countConditions");
  },
  addToSelection: ({ dispatch, commit }, entities) => {
    entities.forEach((entity) => {
      commit("addEntity", entity);
    });
    dispatch("updateSummary");
    dispatch("logAction", { action: "addToSelection", content: entities });
  },
  changeCondition({ state }, params) {
    state.conditions[params.path][params.type][params.value][1] =
      params.reverse;
  },
  changeScope({ state }, scope) {
    Object.keys(state.conditions).forEach((cur) => {
      if (cur !== "lasso") {
        Object.keys(state.conditions[cur]).forEach((type) => {
          Object.keys(state.conditions[cur][type]).forEach((value) => {
            state.conditions[cur][type][value][2] = scope;
          });
        });
      }
    });
  },
  clearConditions({ commit }) {
    commit("clearConditions");
    commit("countConditions");
  },
  clearSelection({ dispatch, commit }) {
    commit("clearSelection");
    dispatch("updateSummary");
    commit("setEntitiesVisible", false);
    state.currentkey = undefined;
    state.exportconditions = {};
    dispatch("logAction", { action: "clearSelection", content: [] });
  },
  computePopularityThreshold({ state, commit }) {
    let sorted = state.entities.sort((a, b) => a[4] - b[4]);
    let index = sorted.length < 50 ? sorted.length - 1 : 50;
    commit("setPopularityThreshold", sorted[index][4]);
  },
  async computeProjection({ commit, dispatch, state }) {
    let selectedpaths = state.paths
      .filter((p) => p.reduction)
      .map((p) => p.path);
    state.loading_projection = true;
    let umap = await fetch(
      `${state.api}/api/reduction/${state.collection}/allpaths`,
      {
        method: "POST",
        body: JSON.stringify({
          selectedpaths,
        }),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      }
    ).then((res) => res.json());

    state.loading_projection = false;
    let coeff = getCoeff(umap.entities, state.widthmap);
    commit("setCoeff", coeff);
    dispatch("setVoronoi", umap.zones);
    commit(
      "setEntities",
      umap.entities.map((e, index) => {
        let newe = e;
        newe[1] = (e[1] - coeff.bounds.minx) * coeff.coeff;
        newe[2] = (e[2] - coeff.bounds.miny) * coeff.coeff;
        newe[4] = state.entities[index][4];
        return newe;
      })
    );
    commit("setSummary", umap.paths);
    commit("setEntitiesLabels", umap.entities);
    commit("setProjectionVisible", false);
    dispatch("logAction", {
      action: "computeProjection",
      content: selectedpaths,
    });
  },
  async dereferenceURI({ commit, state }, URI) {
    commit("setActiveURI", URI);
    if (state.URIDetail[URI]) {
      commit("setActiveURIDetail", state.URIDetail[URI]);
    } else {
      let URIdesc = await fetch(`${state.api}/dereference`, {
        method: "POST",
        body: JSON.stringify({
          URIPath: URI,
          preflanguages: state.preflanguages,
        }),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      }).then((res) => res.json());
      commit("setActiveURIDetail", URIdesc);
      commit("addURIDetail", { URI, URIdesc });
    }
  },
  async displayCollection({ commit, dispatch, state }) {
    state.loading_settings = true;
    let umap = await fetch(`${state.api}/api/load/${state.collection}`, {
      method: "POST",
      body: JSON.stringify({
        preflanguages: state.preflanguages,
        prefixes: state.prefixes,
      }),
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      },
    }).then((res) => res.json());
    state.loading_settings = false;
    let coeff = getCoeff(umap.entities, state.widthmap);
    commit("setCoeff", coeff);
    commit(
      "setEntities",
      umap.entities.map((e) => {
        let newe = e;
        newe[1] = (e[1] - coeff.bounds.minx) * coeff.coeff;
        newe[2] = (e[2] - coeff.bounds.miny) * coeff.coeff;
        return newe;
      })
    );

    commit("setEntitiesLabels", umap.entities);
    dispatch("setVoronoi", umap.zones);
    commit("clearSelection");
    commit("setSummary", umap.paths);
    commit("setHoveredZone", undefined);
    commit("clearConditions");
    commit("clearSelection");
    commit("resetLasso");
    commit("setZoom", [0, 0, 100]);
    dispatch("computePopularityThreshold");
    dispatch("setDefaultColor");
    dispatch("logAction", {
      action: "displayCollection",
      content: state.collection,
    });
  },
  exportSelection({ state }) {
    //
    let csv1 = "URI,Label\n";
    state.selection.forEach(function(row) {
      csv1 += row;
      csv1 += ",";
      csv1 += '"' + state.entitieslabels[row] + '"';
      csv1 += "\n";
    });
    let hiddenElement1 = document.createElement("a");
    hiddenElement1.href = "data:text/csv;charset=utf-8," + encodeURI(csv1);
    hiddenElement1.target = "_blank";
    hiddenElement1.download = "selection.csv";
    hiddenElement1.click();

    let csv2 = "Path,Type,Value,Inverse,Scope\n";
    Object.keys(state.exportconditions).forEach((cur) => {
      if (cur !== "lasso") {
        Object.keys(state.exportconditions[cur]).forEach((type) => {
          Object.keys(state.exportconditions[cur][type]).forEach((value) => {
            csv2 += cur;
            csv2 += ",";
            csv2 += type;
            csv2 += ",";
            csv2 += state.exportconditions[cur][type][value][0];
            csv2 += ",";
            csv2 += state.exportconditions[cur][type][value][1];
            csv2 += ",";
            csv2 += state.exportconditions[cur][type][value][2]
              ? "selection"
              : "whole set";
            csv2 += "\n";
          });
        });
      } else {
        csv2 += "Lasso,,,,\n";
      }
    });
    let hiddenElement2 = document.createElement("a");
    hiddenElement2.href = "data:text/csv;charset=utf-8," + encodeURI(csv2);
    hiddenElement2.target = "_blank";
    hiddenElement2.download = "conditions.csv";
    hiddenElement2.click();
  },
  hoverMissing({ commit, state }, index) {
    commit("setHoveredMissing", index ? state.zones[index].missing : []);
  },
  async initColors({ commit, state }) {
    let colorscale;
    let colorvalues;

    let colors = [
      "#393B79",
      "#637939",
      "#8C6D31",
      "#843C39",
      "#7B4173",
      "#5254A3",
      "#8CA252",
      "#BD9E39",
      "#AD494A",
      "#A55194",
      "#6B6ECF",
      "#B5CF6B",
      "#E7BA52",
      "#D6616B",
      "#CE6DBD",
      "#9C9EDE",
      "#CEDB9C",
      "#E7CB94",
      "#E7969C",
      "#DE9ED6",
    ];
    let values = state.paths[state.color || 0].summaryValues.summary.map(
      (v) => v.value
    );
    commit(
      "setColorFunction",
      d3
        .scaleOrdinal()
        .domain(values)
        .range(colors)
    );
    colorvalues = values;
    colorscale = "ordinal";
    commit("setGradientColors", colors.slice(0, colorvalues.length));

    let options = {
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        pathindex: state.color,
      }),
    };
    let values2 = await fetch(
      `${state.api}/api/values/${state.collection}`,
      options
    ).then((res) => res.json());
    commit(
      "setEntities",
      state.entities.map((entity, index) => {
        let newentity = entity;
        newentity[5] = values2.values[index]
          ? values2.values[index].map((val) => {
              if (colorscale == "linear" || colorvalues.includes(val[0])) {
                return state.colorfunction(val[0]);
              } else {
                return state.colorfunction("other");
              }
            })
          : ["#333"];
        return newentity;
      })
    );
  },
  loadConfig: ({ state }) => {
    let savedCollection = localStorage.getItem("missingCollection");
    if (savedCollection && state.collections.includes(savedCollection))
      state.collection = savedCollection;
    let savedLang = localStorage.getItem("preflanguages");
    if (savedLang && savedLang !== "")
      state.preflanguages = JSON.parse(savedLang);
    let savedURI = localStorage.getItem("missingURIDetail");
    if (savedURI) state.URIDetail = JSON.parse(savedURI);
  },
  async logAction({ state }, params) {
    let user = localStorage.getItem("missingIdentifier")
      ? localStorage.getItem("missingIdentifier")
      : "anonymous";
    let options = {
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        user,
        action: params.action,
        content: params.content,
      }),
    };
    await fetch(`${state.api}/api/log`, options);
    //}
  },
  removeCondition({ commit }, params) {
    commit("removeCondition", params);
    commit("countConditions");
  },
  removeLassoFromConditions({ commit }) {
    commit("removeLassoFromConditions");
    commit("countConditions");
  },
  removeFromSelection({ dispatch, commit }, entities) {
    entities.forEach((entity) => {
      commit("removeEntity", entity);
    });
    dispatch("updateSummary");
    commit("setEntitiesVisible", false);
    dispatch("logAction", { action: "removeFromSelection", content: entities });
  },
  replaceSelection: ({ dispatch, commit }, entities) => {
    commit("clearSelection");
    entities.forEach((entity) => {
      commit("addEntity", entity);
    });
    dispatch("updateSummary");
    dispatch("logAction", { action: "replaceSelection", content: entities });
  },
  async saveProjection({ commit, state }) {
    state.loading_projection = true;
    await fetch(`${state.api}/api/save/${state.collection}`, {
      method: "POST",
      body: JSON.stringify({
        paths: state.paths,
        entities: state.entities,
        zones: {
          center: state.zones.map((z) => z.centerzone),
          missing: state.zones.map((z) => z.missing),
        },
      }),
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    });
    state.loading_projection = false;
    commit("setProjectionVisible", false);
  },
  selectColor({ commit, dispatch }, ind) {
    commit("setColor", ind);
    commit("setOpenPath", ind);
    commit("setColorVisible", false);
    dispatch("initColors");
    dispatch("logAction", { action: "selectColor", content: ind });
  },
  setDefaultColor({ commit, state, dispatch }) {
    let index = state.paths.findIndex((path) => path.color);
    commit("setColor", index);
    dispatch("initColors");
  },
  setLassoFromHoveredZone({ commit, state }) {
    state.zones[state.hoveredzone].polygon.forEach((points) => {
      commit("addToLasso", [
        points[0] + state.widthmarginmap,
        points[1] + state.widthmarginmap,
      ]);
    });
  },
  async setVoronoi({ commit, state }, inputzones) {
    let centerzones = inputzones.center.map((e) => {
      let newe = e;
      newe[0] = (e[0] - state.coeff.bounds.minx) * state.coeff.coeff;
      newe[1] = (e[1] - state.coeff.bounds.miny) * state.coeff.coeff;
      return newe;
    });
    let delaun = Delaunay.from(centerzones);
    let voronoi = delaun.voronoi([
      -1 * state.widthmarginmap,
      -1 * state.widthmarginmap,
      state.widthmap + 2 * state.widthmarginmap,
      state.widthmap + 2 * state.widthmarginmap,
    ]);
    let zones = [];
    for (let i = 0; i < inputzones.center.length; i++) {
      let polygon = voronoi.cellPolygon(i);
      let selection = state.entities
        .filter((entity) => {
          return isInside([entity[1], entity[2]], polygon);
        })
        .map((entity) => entity[0]);
      if (inputzones.missing) {
        zones.push({
          missing: inputzones.missing[i],
          polygon,
          center: centerzones[i],
          centerzone: inputzones.center[i],
        });
      } else {
        let selected = await fetch(
          `${state.api}/api/selection/${state.collection}`,
          {
            method: "POST",
            body: JSON.stringify({
              conditions: [],
              subset: selection,
              missing: true,
              selectedpaths: state.paths
                .filter((p) => p.reduction)
                .map((p) => p.path),
            }),
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Origin": "*",
            },
          }
        ).then((res) => res.json());
        zones.push({
          missing: selected.missing,
          polygon,
          center: centerzones[i],
          centerzone: inputzones.center[i],
        });
      }
    }
    commit("setZones", zones);
  },
  setWidths({ commit }) {
    commit("setWidthType", { type: "", value: window.innerWidth - 10 });
    commit("setWidthType", {
      type: "map",
      value: (window.innerWidth - 10) / 2 - 40,
    });
    commit("setWidthType", { type: "marginmap", value: 20 });
    commit("setWidthType", { type: "pathpattern", value: 19 });
    commit("setWidthType", {
      type: "set",
      value: (window.innerWidth - 10 / 2 - 20) / 2,
    });
  },
  async updateFullSummary({ state }) {
    state.loading_selection = true;

    state.currentkey = "full";
    this._vm.$socket.client.emit("getSummaryWS", {
      identifier: state.collection,
      selectedentities: [],
      prefixes: state.prefixes,
      key: "full",
    });
  },
  async updateSummary({ commit, state }) {
    state.loading_selection = true;
    state.currentkey = `key${Math.random()}`;
    commit("setSelectionSummary", []);
    this._vm.$socket.client.emit("getSummaryWS", {
      identifier: state.collection,
      selectedentities: state.selection,
      prefixes: state.prefixes,
      key: state.currentkey,
    });
  },
  async validateSelection({ state, commit, dispatch }) {
    let results = [];
    let conditions = Object.keys(state.conditions).reduce((acc, cur) => {
      if (cur !== "lasso") {
        Object.keys(state.conditions[cur]).forEach((type) => {
          Object.keys(state.conditions[cur][type]).forEach((value) => {
            acc.push([
              cur,
              type,
              state.conditions[cur][type][value][0],
              state.conditions[cur][type][value][1],
              state.conditions[cur][type][value][2],
              state.conditions[cur][type][value][3],
            ]);
          });
        });
      }
      return acc;
    }, []);
    if (conditions.length > 0) {
      state.loading_selection = true;
      let selected = await fetch(
        `${state.api}/api/selection/${state.collection}`,
        {
          method: "POST",
          body: JSON.stringify({
            conditions,
            subset: state.selection,
          }),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        }
      ).then((res) => res.json());
      results = selected.entities;
    }
    if (state.conditions.lasso) {
      state.conditions.lasso.forEach((ent) => {
        if (!results.includes(ent)) results.push(ent);
      });
    }
    state.exportconditions = state.conditions;
    commit("clearConditions");
    commit("resetLasso");
    commit("countConditions");
    commit("setEntitiesVisible", false);
    commit("setConstraintsVisible", false);
    dispatch("replaceSelection", results);
  },
};

// getters are functions.
const getters = {
  activeURI: (state) => state.activeURI,
  activeURIDetail: (state) => state.activeURIDetail,
  collections: (state) => state.collections,
  collection: (state) => state.collection,
  color: (state) => state.color,
  colorvisible: (state) => state.colorvisible,
  colorfunction: (state) => state.colorfunction,
  conditions: (state) => state.conditions,
  conditionslength: (state) => state.conditionslength,
  constraintsvisible: (state) => state.constraintsvisible,
  entitylabel: (state) => (uri) => state.entitieslabels[uri],
  entities: (state) => state.entities,
  entitiesvisible: (state) => state.entitiesvisible,
  exportconditions: (state) => state.exportconditions,
  gradientcolors: (state) => state.gradientcolors,
  hoveredmissing: (state) => state.hoveredmissing,
  hoveredpath: (state) => state.hoveredpath,
  hoveredzone: (state) => state.hoveredzone,
  label: (state) => state.label,
  lasso: (state) => state.lasso,
  loading_projection: (state) => state.loading_projection,
  loading_selection: (state) => state.loading_selection,
  loading_settings: (state) => state.loading_settings,
  minclusters: (state) => state.minclusters,
  openpath: (state) => state.openpath,
  openpaths: (state) => state.openpaths,
  paths: (state) => state.paths,
  pathsstate: (state) => state.pathsstate,
  pathssubset: (state) => state.pathssubset,
  popularitythreshold: (state) => state.popularitythreshold,
  preflanguages: (state) => state.preflanguages,
  projectionvisible: (state) => state.projectionvisible,
  selection: (state) => state.selection,
  settingsvisible: (state) => state.settingsvisible,
  sliderposition: (state) => state.sliderposition,
  transformMap: (state) => {
    let scale = 100 / state.zoom[2];
    return `translate(${state.widthmarginmap +
      ((-1 * state.widthmap) / 100) *
        state.zoom[0] *
        scale}, ${state.widthmarginmap +
      ((-1 * state.widthmap) / 100) * state.zoom[1] * scale}) scale(${scale})`;
  },
  URIDetail: (state) => state.URIDetail,
  width: (state) => state.width,
  widthmap: (state) => state.widthmap,
  widthmarginmap: (state) => state.widthmarginmap,
  widthset: (state) => state.widthset,
  widthpathpattern: (state) => state.widthpathpattern,
  zones: (state) => state.zones,
  zoom: (state) => state.zoom,
};

const zoomLasso = (coords, zoom, margin) => {
  let scale = 100 / zoom[2];
  let ratio = (window.innerWidth - 10) / 2 / 100;
  return coords.map((coord) => {
    return [
      (coord[0] + zoom[0] * ratio * scale) / scale - margin,
      (coord[1] + zoom[1] * ratio * scale) / scale - margin,
    ];
  });
};

const getCoeff = (vectors, width) => {
  let sortvectors = vectors.map((v) => v);
  let bounds = {};
  sortvectors.sort((a, b) => a[1] - b[1]);
  bounds.minx = Math.floor(sortvectors[1][1]);
  bounds.maxx = Math.ceil(sortvectors[sortvectors.length - 1][1]);
  sortvectors.sort((a, b) => a[2] - b[2]);
  bounds.miny = Math.floor(sortvectors[0][2]);
  bounds.maxy = Math.ceil(sortvectors[sortvectors.length - 1][2]);
  bounds.x = bounds.maxx - bounds.minx;
  bounds.y = bounds.maxy - bounds.miny;
  bounds.side = bounds.x > bounds.y ? bounds.x : bounds.y;
  return { coeff: Math.floor((width * 100) / bounds.side) / 100, bounds };
};

const isInside = (point, polygon) => {
  // ray-casting algorithm based on
  // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
  let x = point[0],
    y = point[1];
  let inside = false;
  for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
    let xi = polygon[i][0],
      yi = polygon[i][1];
    let xj = polygon[j][0],
      yj = polygon[j][1];
    let intersect =
      yi > y != yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
    if (intersect) inside = !inside;
  }
  return inside;
};

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
});
