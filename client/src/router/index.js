import Vue from "vue";
import Router from "vue-router";
import TMP from "@/components/TMP";
import Admin from "@/components/Admin";
import AdminOFF from "@/components/AdminOFF";
import Pid from "@/components/Pid";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "TMP",
      component: TMP,
    },
    {
      path: "/admin",
      name: "Admin",
      component: Admin,
    },
    {
      path: "/adminoff",
      name: "AdminOFF",
      component: AdminOFF,
    },
    {
      path: "/pid",
      name: "Pid",
      component: Pid,
    },
  ],
});
