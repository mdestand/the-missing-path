import Vue from "vue";
// import Vuetify from 'vuetify/lib'
import Element from "element-ui";
import VueSocketIO from "vue-socket.io-extended";
import io from "socket.io-client";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.use(Element);
// Vue.use(Vuetify)

Vue.use(
  VueSocketIO,
  io(process.env.VUE_APP_PROTOCOLWS + "://" + process.env.VUE_APP_SERVER),
  { store }
);

export default new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
