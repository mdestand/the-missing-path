const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");
const api = require("./api");

// Router configuration
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));

// Application initialisation
const app = express();
app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));

app.use(express.static("public"));
app.use("/", router);
app.use("/", api);
app.set("view engine", "pug");

app.listen(8000, () =>
  console.log("ℹ ｢app｣: Paths Analysis' server running on port", 8000)
);
