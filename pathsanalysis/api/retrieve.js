const express = require("express");
const fs = require("fs");
const path = require("path");
//SparqlHttp.URL = URL

const router = express.Router();

router.get("/:type/:identifier", async (req, res) => {
  try {
    let loadfile = await fs.readFile(
      path.join(
        __dirname,
        "../public/files/" +
          req.params.identifier +
          "_" +
          req.params.type +
          ".json"
      ),
      "utf8"
    );
    res.json(loadfile);
  } catch (err) {
    console.error("Error retrieving path summaries", err);
    res.end();
  }
});

module.exports = router;
