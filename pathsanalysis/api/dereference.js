const express = require("express");
const { getLabels } = require("../lib/labelLib");

const router = express.Router();

router.all("/", async (req, res) => {
  // req.setTimeout(600000)
  //req.body.URIPath
  //req.body.prefLanguages

  try {
    let URIPath = req.body.URIPath.replace("<", "").replace(">", "");
    let preflanguages = req.body.preflanguages;
    if (!preflanguages.includes("en")) preflanguages.push("en");

    let thelabels = getLabels(URIPath, preflanguages);
    res.json(thelabels);
    res.end();
  } catch (err) {
    console.error("Error with input parameters", req.params.URIPath, err);
    res.json({ labels: [], comments: [] });
    res.end();
  }
});

module.exports = router;
