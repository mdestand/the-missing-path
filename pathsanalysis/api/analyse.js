const express = require("express");
const fs = require("fs");
const path = require("path");
const fetch = require("isomorphic-fetch");
const SparqlHttp = require("sparql-http-client");
const { getLabels } = require("../lib/labelLib");
SparqlHttp.fetch = fetch;
//SparqlHttp.URL = URL

let datatypesTime = [
  "http://www.w3.org/2001/XMLSchema#integer",
  "http://www.w3.org/2001/XMLSchema#gYear",
  "http://www.w3.org/2001/XMLSchema#boolean",
  "http://www.w3.org/2001/XMLSchema#double",
];

const router = express.Router();

router.get("/", (req, res) => {
  // req.setTimeout(600000)
  res.render("form", { title: "Hey", message: "Hello there!" });
  res.end();
});

router.post("/", async (req, res) => {
  req.setTimeout(0);
  if (!req.body.identifier || !req.body.endpoint || !req.body.maxdepth) {
    let error =
      "You must provide at least an id, an endpoint, a predicate and object, and a depth";
    res.render("form", { title: "Hey", message: "Hello there!", error });
    console.log(req.body.identifier);
    console.log(req.body.endpoint);
    console.log(req.body.maxdepth);
    console.error(error);
    res.json({ error });
    res.end();
  } else {
    console.log("Retrieve path summaries", new Date());
    let params = {
      endpoint: req.body.endpoint,
      pred: req.body.pred,
      obj: req.body.obj,
      constraint: req.body.constraint,
      step: Number(req.body.step),
      step2: Number(req.body.step2),
      maxdepth: Number(req.body.maxdepth),
      maxvaluessummary: Number(req.body.maxvaluessummary) || 100,
      minquota: Number(req.body.minquota) || undefined,
      maxquota: Number(req.body.maxquota) || 10000,
    };
    try {
      let rawpaths;
      let fullpaths;
      let pathsummary;
      let entities;
      let entitiespathsummary;
      let countentities;
      console.log(params.step, params.step === 1, params);
      if (params.step === 1) {
        rawpaths = await getRawPaths(params, 1, {});
        console.log("rawpaths", rawpaths);
        console.log("rawpaths length", rawpaths["1"].length);
        writeFile(req.body.identifier + "_rawpaths.json", { rawpaths });
        //res.render('form', { title: 'Hey', message: 'Hello there!', rawpaths: '/files/'+ rawpathsfilename})

        params.step++;
      }
      if (params.step === 2 && params.step2 >= 2) {
        if (!rawpaths)
          rawpaths = await readFile(req.body.identifier, "rawpaths");
        console.log("rawpaths length", rawpaths["1"].length);
        countentities = await countEntities(params);
        console.log("countentities", countentities);
        //
        fullpaths = await getFullPaths(params, rawpaths, countentities);
        writeFile(req.body.identifier + "_fullpaths.json", { fullpaths });
        params.step++;
      }
      if (params.step === 3 && params.step2 >= 3) {
        if (!fullpaths)
          fullpaths = await readFile(req.body.identifier, "fullpaths");
        fullpaths = await getPathsLabels(fullpaths);
        writeFile(req.body.identifier + "_fullpaths.json", { fullpaths });
        params.step++;
      }
      if (params.step === 4 && params.step2 >= 4) {
        console.log("fullpaths", fullpaths);
        if (!fullpaths) {
          fullpaths = await readFile(req.body.identifier, "fullpaths");
          console.log("fullpaths", fullpaths);
        }
        if (!countentities) countentities = await countEntities(params);
        if (!params.minquota) params.minquota = countEntities * 0.05;
        //
        pathsummary = await getPathSummaryForEntities(params, fullpaths);
        writeFile(req.body.identifier + "_pathsummary.json", { pathsummary });
        params.step++;
      }
      if (params.step === 5 && params.step2 >= 5) {
        if (!pathsummary)
          pathsummary = await readFile(req.body.identifier, "pathsummary");
        entities = await getEntities(params, pathsummary);
        console.log("///", entities);
        writeFile(req.body.identifier + "_entities.json", { entities });
        //res.render('form', { title: 'Hey', message: 'Hello there!', rawpaths, entities})
        params.step++;
      }
      if (params.step === 6 && params.step2 >= 6) {
        if (!entities)
          entities = await readFile(req.body.identifier, "entities");
        if (!fullpaths)
          fullpaths = await readFile(req.body.identifier, "fullpaths");
        entitiespathsummary = await getEntitiesPathSummary(
          params,
          entities,
          fullpaths
        );
        writeFile(req.body.identifier + "_entitiespathsummary.json", {
          entitiespathsummary,
        });
        params.step++;
      }
      if (params.step === 7 && params.step2 >= 7) {
        if (!entities)
          entities = await readFile(req.body.identifier, "entities");
        if (!fullpaths)
          fullpaths = await readFile(req.body.identifier, "fullpaths");
        if (!entitiespathsummary)
          entitiespathsummary = await readFile(
            req.body.identifier,
            "entitiespathsummary"
          );
        //entitiespathsummary = transformEntitiesPathSummary(entitiespathsummary)
      }
      //res.render('form', { title: 'Hey', message: 'Hello there!', rawpaths, entities, fullpaths, fullentities})
      res.json({ fullpaths, pathsummary, entitiespathsummary });
      res.end();
    } catch (err) {
      console.error("Error retrieving path summaries", err);
      res.end();
    }
  }
});

const pathToList = (path) => {
  if (path === "") return [];
  path = path.substr(1, path.length - 2);
  return path.split(">/<");
};
const listToPath = (list) => {
  return "<" + list.join(">/<") + ">";
};

const writeFile = (filename, content) => {
  console.log("writeFile", filename);
  try {
    let filepath = path.join(__dirname, "../public/files/" + filename);
    fs.writeFile(filepath, JSON.stringify(content));
  } catch (err) {
    console.error("Impossible to write file ", err);
  }
};

const readFile = async (id, type) => {
  try {
    let loadfile = await fs.readFile(
      path.join(__dirname, "../public/files/" + id + "_" + type + ".json"),
      "utf8"
    );
    return JSON.parse(loadfile)[type];
  } catch (err) {
    console.error("Impossible to read file ", err);
  }
};

const makeFindPathsQuery = (initialPath, params) => {
  let pathQuery = "";
  let depth = initialPath.length + 1;

  let { pred, obj, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  for (let i = 1; i <= depth; i++) {
    if (initialPath[i - 1]) {
      pathQuery += "<" + initialPath[i - 1] + "> ?o" + i + ". ";
    } else {
      pathQuery += " ?property" + " ?o" + i + ". ";
    }
    if (i < depth) pathQuery += " ?o" + i;
  }
  return `SELECT DISTINCT ?property WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`;
};

const makeCoverageQuery = (initialPath, params) => {
  let pathQuery = "";
  let { pred, obj, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  let values;
  let depth = initialPath.length;
  for (let i = 1; i <= depth; i++) {
    pathQuery += " <" + initialPath[i - 1] + "> ?o" + i + ". ";
    if (i < depth) {
      pathQuery += " ?o" + i;
    } else {
      values = " ?o" + i;
    }
  }
  console.log(`SELECT (COUNT(DISTINCT ?s) AS ?uniqueEntities) (COUNT(DISTINCT ${values}) AS ?uniqueEntitiesValues) (COUNT(?s) AS ?total) WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`);
  return `SELECT (COUNT(DISTINCT ?s) AS ?uniqueEntities) (COUNT(DISTINCT ${values}) AS ?uniqueEntitiesValues) (COUNT(?s) AS ?total) WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`;
};

const makeSummaryQuery = (initialPath, params) => {
  let pathQuery = "";
  let { pred, obj, maxvaluessummary, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  let values;
  let depth = initialPath.length;
  for (let i = 1; i <= depth; i++) {
    pathQuery += " <" + initialPath[i - 1] + "> ?o" + i + ". ";
    if (i < depth) {
      pathQuery += " ?o" + i;
    } else {
      values = " ?o" + i;
    }
  }
  let query = `SELECT DISTINCT ?values (COUNT(?values) AS ?count) WHERE {
            ${constraintquery}
            ?s ${pathQuery}
            BIND(${values} as ?values) .
        } GROUP BY ?values ORDER BY DESC(?count) LIMIT ${maxvaluessummary}`;
  console.log(query);
  return query;
};

const makeEntitiesQuery = (initialPath, value, params) => {
  let pathQuery = "";
  let { pred, obj, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  let dt =
    value.datatype && value.datatype !== "" ? "^^<" + value.datatype + ">" : "";
  for (let i = 1; i <= initialPath.length; i++) {
    pathQuery += ` <${initialPath[i - 1]}> ?o${i} . `;
    if (i === initialPath.length) {
      pathQuery += `FILTER ( ?o${i} IN ('${value.value}'${dt}) )`;
    }
  }
  console.log(`SELECT DISTINCT ?s WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`);
  return `SELECT DISTINCT ?s WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`;
};

const makeOtherEntitiesQuery = (initialPath, values, params) => {
  let pathQuery = "";
  let { pred, obj, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  let valueslist = values
    .map((val) => {
      let dt =
        val.datatype && val.datatype !== "" ? "^^<" + val.datatype + ">" : "";
      return `'${val.value}'${dt}`;
    })
    .join(", ");
  for (let i = 1; i <= initialPath.length; i++) {
    pathQuery += ` <${initialPath[i - 1]}> ?o${i} . `;
    if (i === initialPath.length) {
      pathQuery += `FILTER ( ?o${i} NOT IN(${valueslist}) )`;
    }
  }
  console.log(`SELECT DISTINCT ?s WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`);
  return `SELECT DISTINCT ?s WHERE {
        ${constraintquery}
        ?s ${pathQuery}
    }`;
};

const makeRestEntitiesQuery = (initialPath, params) => {
  let pathQuery = "";
  let { pred, obj, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  for (let i = 1; i <= initialPath.length; i++) {
    pathQuery += ` <${initialPath[i - 1]}> ?o${i} . `;
  }
  console.log(`SELECT DISTINCT ?s WHERE {
        ${constraintquery}
        FILTER( NOT EXISTS { ?s ${pathQuery} })
    }`);
  return `SELECT DISTINCT ?s WHERE {
        ${constraintquery}
        FILTER( NOT EXISTS { ?s ${pathQuery} })
        
    }`;
};

const makeEntityQuery = (entity, depth, params) => {
  let pathvar = "?values ?p1 ";
  let pathQuery = "<" + entity + "> ?p1 ?o1 . ";
  let { pred, obj, maxdepth, maxvaluessummary } = params;
  let values;
  for (let i = 2; i <= depth; i++) {
    pathvar += "?p" + i + " ";
    pathQuery += " ?o" + (i - 1) + " ?p" + i + " ?o" + i + " . ";
  }
  console.log(`SELECT DISTINCT ${pathvar} WHERE {
        ${pathQuery}
        BIND(?o${depth} as ?values) .
        BIND(DATATYPE(?o${depth}) as ?datatype) .
        BIND(LANG(?o${depth}) as ?lang)
    }`);
  return `SELECT DISTINCT ${pathvar} WHERE {
        ${pathQuery}
        BIND(?o${depth} as ?values) .
        BIND(DATATYPE(?o${depth}) as ?datatype) .
        BIND(LANG(?o${depth}) as ?lang)
    }`;
};

const countEntities = async (params) => {
  let { endpoint, pred, obj, constraint } = params;
  let constraintquery = constraint ? constraint : `?s <${pred}> <${obj}> .`;
  let query = `SELECT (COUNT(DISTINCT ?s) AS ?total) WHERE {
        ${constraintquery}
    }`;
  let total = await getData(endpoint, query, {});
  //console.log(total.results.bindings[0])
  return Number(total.results.bindings[0].total.value);
};

const getRawPaths = async (params, depth, rawpaths) => {
  let { endpoint, maxdepth } = params;
  let newpaths = [];
  //console.log(params, depth, rawpaths)
  if (depth === 1) {
    let newextensions = await getData(
      endpoint,
      makeFindPathsQuery([], params),
      {}
    );
    //console.log(newextensions)
    newextensions.results.bindings.forEach((ext) => {
      newpaths.push(listToPath([ext.property.value]));
    });
  } else {
    let previouspaths = rawpaths[depth - 1];
    for (let i = 1; i <= previouspaths.length; i++) {
      //console.log(previouspaths[i-1])
      let previouspath = pathToList(previouspaths[i - 1]);
      let newextensions = await getData(
        endpoint,
        makeFindPathsQuery(previouspath, params),
        {}
      );
      if (newextensions) {
        newextensions.results.bindings.forEach((ext) => {
          newpaths.push(listToPath([...previouspath, ext.property.value]));
        });
      }
    }
  }
  rawpaths[depth] = newpaths;
  return depth < maxdepth ? getRawPaths(params, depth + 1, rawpaths) : rawpaths;
};

const getFullPaths = async (params, rawpaths, countTotalEntities) => {
  console.log("getFullPaths");
  let { endpoint, maxdepth } = params;
  let fullpaths = [];
  for (let i = 1; i <= maxdepth; i++) {
    console.log(i);
    for (let j = 0; j < rawpaths[i].length; j++) {
      console.log(i, j);
      //coverage
      try {
        let coverage = await getData(
          endpoint,
          makeCoverageQuery(pathToList(rawpaths[i][j]), params),
          {}
        );
        if (coverage) {
          fullpaths.push({
            path: rawpaths[i][j],
            uniqueEntities: Number(
              coverage.results.bindings[0].uniqueEntities.value
            ),
            total: Number(coverage.results.bindings[0].total.value),
            uniqueEntitiesValues: Number(
              coverage.results.bindings[0].uniqueEntitiesValues.value
            ),
            countTotalEntities,
          });
        }
      } catch (err) {
        console.error("Error retrieving full paths", err);
      }
    }
  }
  console.log(fullpaths);
  return fullpaths.sort((a, b) => {
    if (b.uniqueEntities === a.uniqueEntities) {
      a.uniqueEntitiesValues - b.uniqueEntitiesValues;
    } else {
      return b.uniqueEntities - a.uniqueEntities;
    }
  });
};

const getPathsLabels = async (paths) => {
  console.log("getPathsLabels", paths);
  let newpaths = [];
  for (let i = 0; i < paths.length; i++) {
    console.log("path", i);
    //coverage
    try {
      let labels = await getLabels(
        paths[i].path.replace("<", "").replace(">", ""),
        []
      );
      //console.log(labels)
      let newpath = paths[i];
      newpath.label =
        labels && labels.labels.length > 0 ? labels.labels[0] : "";
      newpaths.push(newpath);
    } catch (err) {
      newpaths.push(paths[i]);
      console.error("Error retrieving full paths", err);
    }
  }
  return newpaths;
};

const getPathSummaryForEntities = async (params, fullpaths) => {
  console.log("getPathSummary");
  let { endpoint, maxquota, maxvaluessummary, minquota, pred } = params;
  let pathsummary = [];
  let returnpath = {};
  for (let j = 0; j < fullpaths.length; j++) {
    let path = fullpaths[j];
    if (pathToList(path.path)[0] !== pred) {
      let summary = await getData(
        endpoint,
        makeSummaryQuery(pathToList(path.path), params, "values"),
        {}
      );
      //if (summary) console.log('bindings,', summary.results.bindings.length, 'count first', Number(summary.results.bindings[0].count.value))
      if (
        summary &&
        summary.results.bindings.length > 0 &&
        summary.results.bindings.length < maxvaluessummary &&
        summary.results.bindings[0].count.value >= minquota &&
        Number(summary.results.bindings[0].count.value) <= maxquota
      ) {
        returnpath = path;
        returnpath.summaryvalues = summary.results.bindings.map((res) => {
          return {
            value: res.values.value,
            count: res.count.value,
            datatype: res.values.datatype || null,
            lang: res.values["xml:lang"] || null,
          };
        });
        break;
      }
    }
  }
  return returnpath;
};

const getEntities = async (params, selectedpath) => {
  let { endpoint } = params;
  console.log("pathsummary", selectedpath);
  let entities = new Set();
  for (let j = 0; j < selectedpath.summaryvalues.length; j++) {
    let partialentities = await getData(
      endpoint,
      makeEntitiesQuery(
        pathToList(selectedpath.path),
        selectedpath.summaryvalues[j],
        params
      ),
      {}
    );
    if (partialentities && partialentities.results) {
      partialentities.results.bindings.forEach((res) => {
        entities.add(res.s.value);
      });
    }
  }
  //
  let otherentities = await getData(
    endpoint,
    makeOtherEntitiesQuery(
      pathToList(selectedpath.path),
      selectedpath.summaryvalues,
      params
    ),
    {}
  );
  if (otherentities && otherentities.results) {
    otherentities.results.bindings.forEach((res) => {
      entities.add(res.s.value);
    });
  }
  //
  let restentities = await getData(
    endpoint,
    makeRestEntitiesQuery(pathToList(selectedpath.path), params),
    {}
  );
  if (restentities && restentities.results) {
    restentities.results.bindings.forEach((res) => {
      entities.add(res.s.value);
    });
  }
  //console.log(pathsummary)
  return [...entities];
};

const getEntitiesPathSummary = async (params, entities, pathsummary) => {
  let { endpoint, maxdepth } = params;
  let pathsindex = pathsummary.reduce((acc, cur, ind) => {
    acc[cur.path] = ind;
    return acc;
  }, {});
  let fullentities = [];
  for (let j = 0; j < entities.length; j++) {
    let entity = {};
    for (let i = 1; i <= maxdepth; i++) {
      try {
        let entitydesc = await getData(
          endpoint,
          makeEntityQuery(entities[j], i, params),
          {}
        );
        if (entitydesc) {
          entitydesc.results.bindings.forEach((res) => {
            let path = [];
            for (let k = 1; k <= i; k++) {
              path.push(res["p" + k].value);
            }
            let realpath = listToPath(path);
            if (!entity[realpath]) entity[realpath] = [];
            entity[realpath].push([
              res.values.value,
              res.values.datatype ? res.values.datatype : null,
              res.values["xml:lang"] ? res.values["xml:lang"] : null,
            ]);
          });
        }
      } catch (err) {
        console.error("Error retrieving path summaries", err);
      }
    }
    fullentities.push({
      uri: entities[j],
      paths: pathsummary.map((path) => {
        return entity[path.path] ? entity[path.path] : null;
      }),
    });
  }
  console.log(fullentities);
  return fullentities;
};

/*const transformEntitiesPathSummary = async (entitiespathsummary) => {
    entitiespathsummary = entitiespathsummary.map(entity => {
        return {
            ...entity,
            paths: entity.path.map(path => {
                let dictdatatypes = {}
                let dictlanguages = {}
                path[1].forEach(dt => {
                    dictdatatypes[dt]
                })
                return [
                    path[0],
                    path[1],
                    path[2]
                ]
            })
        }
    })
    let fullentities = []
    for (let j = 0; j < entities.length; j++) {
        let entity = {}
        for (let i = 1; i <= maxdepth; i++) {
            try{
                let entitydesc = await getData(endpoint, makeEntityQuery(entities[j], i, params), {})
                if (entitydesc) {
                    entitydesc.results.bindings.forEach(res => {
                        let path = []
                        for (let k = 1; k <= i; k++) {
                            path.push(res['p' + k].value)
                        }
                        let realpath = listToPath(path)
                        if (!entity[realpath]) entity[realpath] = []
                        entity[realpath].push([res.values.value, res.values.datatype ? res.values.datatype : null, res.values['xml:lang'] ? res.values['xml:lang'] : null])
                    })
                }
            }
            catch (err) {
                console.error('Error retrieving path summaries', err)
            }
        }
        fullentities.push({
            uri: entities[j],
            paths: pathsummary.map(path => {
                return entity[path.path] ? entity[path.path] : null
            })
        })
    }
    console.log(fullentities)
    return fullentities
}*/

const getData = async (endpoint, query, prefixes) => {
  try {
    console.log("ici getData", endpoint, prefixes, query);
    let req = new SparqlHttp({
      endpointUrl: endpoint,
      headers: { Accept: "application/sparql-results+json" },
    });
    let res = await req.selectQuery(query);
    let toparse = await res.text();
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 2000);
    });
    //console.log('toto',toparse)
    try {
      return JSON.parse(toparse);
    } catch (e) {
      return JSON.parse(JSON.stringify(toparse));
    }
    //if (toparse)
  } catch (err) {
    console.error("Error retrieving data", err);
    return undefined;
  }
};

/*
export const getLabels = async (urisToLabel, prefixes) => {
    let missingUris
    // create a local graph
    let graph = new rdflib.IndexedFormula()

    propsInDatabase.forEach((prop, index) => {
        if (prop) {
            if (prop.label && prop.label !== '') {
                urisToLabel[index].label = prop.label
            } else if (prop.loadAttempts > 3 && new Date(prop.modifiedAt) > limitDate) {
                urisToLabel[index].label = getPropName(prop.property)
            }
            urisToLabel[index].comment = prop.comment
        }
    })
    missingUris = urisToLabel.filter(prop => !prop.label)
    //console.log(missingUris)
    // load
    if (missingUris.length > 0) {
        await Promise.all(missingUris.map(prop => loadUri(prop.uri, graph)).map(ignorePromise))
        missingUris = await Promise.all(getLabelsFromGraph(missingUris, graph))
        propertyModel.createOrUpdate(missingUris)
    }
    //console.log(missingUris)
    //
    return urisToLabel.map(prop => {
        if (prop.label) {
            return prop
        } else {
            // console.log('ICI prop', prop)
            const missing = missingUris.filter(missingprop => missingprop.uri === prop.uri && missingprop.label)
            if (missing.length > 0) {
                return missing[0]
            } else {
                return { ...prop, label: getPropName(prop.uri) }
            }
        }
    })
}*/

module.exports = router;
