import express from "express";
import bodyParser from "body-parser";
import fetch from "node-fetch";
import path from "path";
import { promises as fs } from "fs";

let pathstoanalyse = [
  { pathlist: ["additives_n"], type: "number" },
  { pathlist: ["additives_tags"], type: "str" },
  { pathlist: ["allergens_tags"], type: "str" },
  { pathlist: ["brands_tags"], type: "str" },
  { pathlist: ["categories_tags"], type: "str" },
  { pathlist: ["completeness"], type: "number" },
  { pathlist: ["countries_tags"], type: "str" },
  { pathlist: ["countries_imported"], type: "str" },
  { pathlist: ["creator"], type: "str" },
  { pathlist: ["data_quality_tags"], type: "str" },
  { pathlist: ["data_quality_bugs_tags"], type: "str" },
  { pathlist: ["data_quality_errors_tags"], type: "str" },
  { pathlist: ["data_quality_info_tags"], type: "str" },
  { pathlist: ["data_quality_warnings_tags"], type: "str" },
  { pathlist: ["data_sources_tags"], type: "str" },
  { pathlist: ["editors_tags"], type: "str" },
  { pathlist: ["informers_tags"], type: "str" },
  { pathlist: ["correctors_tags"], type: "str" },
  { pathlist: ["fruits-vegetables-nuts_100g_estimate"], type: "number" },
  { pathlist: ["image_front_url"], type: "str" },
  { pathlist: ["image_ingredients_url"], type: "str" },
  { pathlist: ["image_nutrition_url"], type: "str" },
  { pathlist: ["ingredients_from_palm_oil_tags"], type: "str" },
  { pathlist: ["ingredients_analysis_tags"], type: "str" },
  { pathlist: ["ingredients_n"], type: "number" },
  { pathlist: ["ingredients_tags"], type: "str" },
  { pathlist: ["labels_tags"], type: "str" },
  { pathlist: ["minerals_tags"], type: "str" },
  { pathlist: ["nova_group"], type: "number" },
  { pathlist: ["nucleotides_tags"], type: "str" },
  { pathlist: ["nutrient_levels_tags"], type: "str" },
  {
    pathlist: [
      "nutriments",
      [
        "fat_unit",
        "vitamin-a_unit",
        "salt_unit",
        "vitamin-c_unit",
        "calcium_unit",
        "fiber_unit",
        "energy-kcal_unit",
        "sodium_unit",
        "trans-fat_unit",
        "iron_unit",
        "energy_unit",
        "proteins_unit",
        "cholesterol_unit",
        "carbohydrates_unit",
      ],
    ],
    type: "str",
  },
  {
    pathlist: [
      "nutriments",
      [
        "iron",
        "vitamin-c",
        "energy_100g",
        "fiber",
        "fat_value",
        "sugars",
        "vitamin-a_100g",
        "trans-fat",
        "energy-kcal_100g",
        "nutrition-score-fr_serving",
        "vitamin-c_serving",
        "vitamin-c_100g",
        "energy-kcal_serving",
        "vitamin-a",
        "energy_value",
        "iron_value",
        "calcium_value",
        "salt",
        "proteins_value",
        "proteins_serving",
        "vitamin-a_value",
        "fat_serving",
        "energy_serving",
        "fat",
        "energy-kcal_value",
        "sodium_serving",
        "nova-group_100g",
        "carbohydrates",
        "salt_100g",
        "fat_100g",
        "sodium_value",
        "proteins",
        "sugars_value",
        "saturated-fat",
        "saturated-fat_100g",
        "fiber_serving",
        "calcium_serving",
        "iron_100g",
        "cholesterol",
        "sodium",
        "energy",
        "fiber_100g",
        "trans-fat_100g",
        "energy-kcal",
        "nutrition-score-fr",
        "cholesterol_serving",
        "cholesterol_100g",
        "trans-fat_serving",
        "sugars_100g",
        "fiber_value",
        "salt_serving",
        "cholesterol_value",
        "carbohydrates_100g",
        "calcium",
        "sodium_100g",
        "proteins_100g",
        "iron_serving",
        "nova-group",
        "trans-fat_value",
        "nutrition-score-fr_100g",
        "saturated-fat_value",
        "vitamin-c_value",
        "carbohydrates_serving",
        "carbohydrates_value",
        "vitamin-a_serving",
        "calcium_100g",
        "salt_value",
        "saturated-fat_serving",
        "fruits-vegetables-nuts-estimate-from-ingredients_100g",
        "nova-group_serving",
        "sugars_serving",
      ],
    ],
    type: "number",
  },
  { pathlist: ["nutriscore_data", "grade"], type: "str" },
  {
    pathlist: [
      "nutriscore_data",
      [
        "is_fat",
        "proteins_points",
        "fiber_value",
        "positive_points",
        "energy_value",
        "energy_points",
        "negative_points",
        "score",
        "energy",
        "fruits_vegetables_nuts_colza_walnut_olive_oils_points",
        "is_cheese",
        "sodium",
        "saturated_fat_ratio_value",
        "saturated_fat_ratio",
        "sugars_value",
        "saturated_fat_value",
        "saturated_fat_ratio_points",
        "fiber",
        "proteins",
        "sugars",
        "sodium_value",
        "sodium_points",
        "is_water",
        "is_beverage",
        "saturated_fat",
        "sugars_points",
        "saturated_fat_points",
        "fiber_points",
        "fruits_vegetables_nuts_colza_walnut_olive_oils",
        "fruits_vegetables_nuts_colza_walnut_olive_oils_value",
        "proteins_value",
      ],
    ],
    type: "number",
  },
  { pathlist: ["nutriscore_score"], type: "number" },
  { pathlist: ["nutrition_score_beverage"], type: "number" },
  { pathlist: ["nutrition_data_prepared_per"], type: "str" },
  { pathlist: ["nutrition_grades_tags"], type: "str" },
  { pathlist: ["origins"], type: "str" },
  { pathlist: ["packaging_tags"], type: "str" },
  { pathlist: ["popularity_tags"], type: "str" },
  { pathlist: ["product_name"], type: "str" },
  { pathlist: ["product_quantity"], type: "number" },
  { pathlist: ["states_tags"], type: "str" },
  { pathlist: ["traces_from_ingredients"], type: "str" },
  { pathlist: ["unique_scans_n"], type: "number" },
];
const apioff = "https://world.openfoodfacts.org/api/v0/product/";

const urioff = "https://world.openfoodfacts.org/product/";

const router = express.Router();

router.use(bodyParser.json({ limit: "50mb", extended: true }));
router.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "50mb",
    parameterLimit: 1000000,
  })
);

router.get("/", (req, res) => {
  // req.setTimeout(600000)
  res.render("form", { title: "Hey", message: "Hello there!" });
  res.end();
});

router.post("/", async (req, res) => {
  req.setTimeout(0);
  if (!req.body.identifier || !req.body.codes) {
    let error = "You must provide at least an id, and a list of codes";
    console.error(error, req.body);
    res.json({ error });
    res.end();
  } else {
    console.log("Retrieve OFF summaries", new Date());
    let params = {
      codes: req.body.codes,
      step: Number(req.body.step),
    };
    //console.log(params.codes)
    if (!Array.isArray(params.codes)) {
      const regex = RegExp(/^\d{1,}$/);
      let splitcodes = params.codes.split("\r\n");
      if (splitcodes.length === 1) {
        splitcodes = params.codes.split("\n");
        if (splitcodes.length === 1) {
          splitcodes = params.codes.split("\r");
          if (splitcodes.length === 1) {
            splitcodes = params.codes.split(",");
          }
        }
      }
      console.log("splitcodes", splitcodes);
      params.codes = splitcodes.filter((code) => {
        return regex.test(code);
      });
    }
    console.log("params.codes", params.codes);
    try {
      let rawpaths = [];
      let fullpaths;
      let pathsummary;
      let entities;
      let entitiespathsummary;
      let countentities;
      console.log(params.step, params.step === 1, params);
      if (params.step === 1) {
        fullpaths = [];
        pathstoanalyse.forEach((pathentry) => {
          let pathname = "";
          let pathchunks = [];
          pathentry.pathlist.forEach((pathpart, index) => {
            if (Array.isArray(pathpart)) {
              pathpart.forEach((option) => {
                rawpaths.push({
                  path: pathname + "<" + option + ">",
                  type: pathentry.type,
                  chuncks: [...pathchunks, option],
                });
              });
            } else {
              pathname += "<" + pathpart + ">";
              pathchunks.push(pathpart);
              if (index < pathentry.pathlist.length - 1) {
                pathname += "/";
              } else {
                rawpaths.push({
                  path: pathname,
                  type: pathentry.type,
                  chuncks: pathchunks,
                });
              }
            }
          });
        });

        entitiespathsummary = await getEntitiesPathSummary(params, rawpaths);
        let ordered = orderSummaryEntities(rawpaths, entitiespathsummary);
        console.log("Salut");
        console.log(
          "ordered.rawpaths",
          ordered.rawpaths.length,
          "ordered.entitiespathsummary",
          ordered.fullentities.length
        );
        //console.log('ordered.entitiespathsummary', ordered.entitiespathsummary)
        entitiespathsummary = ordered.fullentities;
        fullpaths = ordered.rawpaths.map((path) => {
          return {
            path: path.path,
            prefixed: path.path,
            reduction: false,
            color: false,
          };
        });
        writeFile(req.body.identifier + "_fullpaths.json", { fullpaths });

        writeFile(req.body.identifier + "_entitiespathsummary.json", {
          entitiespathsummary,
        });
        params.step++;
      }
      if (params.step === 2) {
        if (!fullpaths)
          fullpaths = await readFile(req.body.identifier, "fullpaths");
        if (!entitiespathsummary)
          entitiespathsummary = await readFile(
            req.body.identifier,
            "entitiespathsummary"
          );
        //entitiespathsummary = transformEntitiesPathSummary(entitiespathsummary)
      }
      res.json({ fullpaths, entitiespathsummary });
      res.end();
    } catch (err) {
      console.error("Error retrieving path summaries", err);
      res.end();
    }
  }
});

const writeFile = (filename, content) => {
  console.log("writeFile", filename);
  try {
    let filepath = path.join(__dirname, "../public/files/" + filename);
    fs.writeFile(filepath, JSON.stringify(content));
  } catch (err) {
    console.error("Impossible to write file ", err);
  }
};

const readFile = async (id, type) => {
  try {
    let loadfile = await fs.readFile(
      path.join(__dirname, "../public/files/" + id + "_" + type + ".json"),
      "utf8"
    );
    return JSON.parse(loadfile)[type];
  } catch (err) {
    console.error("Impossible to read file ", err);
  }
};

const getEntitiesPathSummary = async (params, rawpaths) => {
  try {
    let { codes } = params;
    let fullentities = [];
    for (let j = 0; j < codes.length; j++) {
      let entitypaths = {};
      let url = apioff + codes[j] + ".json";
      let uri = urioff + codes[j];
      let entitydesc = await getData(url);
      //console.log(uri, entitydesc)
      //console.log(rawpaths)
      for (let i = 0; i < rawpaths.length; i++) {
        console.log(rawpaths[i]);
        //find the path in json object
        let varpath = entitydesc.product;
        rawpaths[i].chuncks.forEach((chunck) => {
          if (varpath) varpath = varpath[chunck];
        });
        console.log(varpath);
        // the entity has not this path
        let thispathvalues;
        if (!varpath) {
          thispathvalues = null;
        } else {
          if (rawpaths[i].type === "str") {
            if (Array.isArray(varpath)) {
              if (varpath.length > 0) {
                thispathvalues = varpath.map((val) => {
                  return [val, "str", null];
                });
              } else {
                thispathvalues = null;
              }
            } else {
              thispathvalues = [[varpath, "str", null]];
            }
          } else if (rawpaths[i].type === "number") {
            if (Array.isArray(varpath)) {
              if (varpath.length > 0) {
                thispathvalues = varpath.map((val) => {
                  return [val, "number", null];
                });
              } else {
                thispathvalues = null;
              }
            } else {
              thispathvalues = [[varpath, "number", null]];
            }
          }
        }
        entitypaths[rawpaths[i].path] = thispathvalues;
      }
      fullentities.push({
        uri,
        namedpaths: entitypaths,
      });
    }
    console.log("fullentities", fullentities.length);
    return fullentities;
  } catch (err) {
    console.error("Error getting path summaries ", err);
  }
};

const orderSummaryEntities = (rawpaths, fullentities) => {
  rawpaths = rawpaths
    .map((path) => {
      let values = fullentities
        .map((entity) => entity.namedpaths[path.path])
        .filter((entity) => entity);
      return {
        path: path.path,
        type: path.type,
        chuncks: path.chunks,
        countTotalEntities: values.length,
      };
    })
    .filter((path) => path.countTotalEntities > 0)
    .sort((a, b) => {
      if (a.countTotalEntities === b.countTotalEntities) {
        return a.path.localeCompare(b.path);
      } else {
        return b.countTotalEntities - a.countTotalEntities;
      }
    });
  fullentities = fullentities.map((entity) => {
    let entitypaths = rawpaths.map((path) => entity.namedpaths[path.path]);
    return {
      uri: entity.uri,
      paths: entitypaths,
    };
  });
  return { rawpaths, fullentities };
};

const getData = async (url) => {
  try {
    const response = await fetch(url);
    const json = await response.json();
    return json;
  } catch (err) {
    console.error("Error retrieving data", err);
    return undefined;
  }
};

export default router;
