const express = require("express");
const analyse = require("./analyse");
const dereference = require("./dereference");
// const openfoodfacts = require("./openfoodfacts");

const router = express.Router();

// middleware to use for all requests
router.use("/", (req, res, next) => {
  if (req.path === "/favicon.ico") {
    res.writeHead(200, { "Content-Type": "image/x-icon" });
    res.end();
  }
  next();
});

router.use("/analyse", analyse);
router.use("/dereference", dereference);
// router.use("/openfoodfacts", openfoodfacts);

module.exports = router;
