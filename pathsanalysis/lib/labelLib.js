const rdflib = require("rdflib");

const getLabels = async (URIPath, preflanguages) => {
  return new Promise((resolve, reject) => {
    try {
      let graph = new rdflib.IndexedFormula();
      let fetcher = new rdflib.Fetcher(graph, 5000);
      fetcher.nowOrWhenFetched(URIPath, (ok, body, xhr) => {
        if (!ok) {
          console.log("Oops, something happened and couldn't fetch data");
          resolve({ labels: [], comments: [] });
        } else {
          let labels = getDetailsFromGraph(
            URIPath,
            graph,
            "labels",
            preflanguages
          );
          let comments = getDetailsFromGraph(
            URIPath,
            graph,
            "comments",
            preflanguages
          );
          //console.log(labels, comments)
          if (labels.length < 1 && comments.length < 1) {
            URIPath = URIPath.replace(
              "www.wikidata.org/prop/direct/",
              "www.wikidata.org/wiki/Property:"
            );
            labels = getDetailsFromGraph(
              URIPath,
              graph,
              "labels",
              preflanguages
            );
            comments = getDetailsFromGraph(
              URIPath,
              graph,
              "comments",
              preflanguages
            );
            //console.log('next 1', URIPath,labels, comments)
            if (labels.length < 1 && comments.length < 1) {
              URIPath = URIPath.replace("http://", "https://");
              labels = getDetailsFromGraph(
                URIPath,
                graph,
                "labels",
                preflanguages
              );
              comments = getDetailsFromGraph(
                URIPath,
                graph,
                "comments",
                preflanguages
              );
              //console.log('next 2', URIPath,labels, comments)
            }
          }
          resolve({ labels, comments });
        }
      });
    } catch (err) {
      console.error("Error dereferencing URI", req.params.URIPath, err);
      reject({ labels: [], comments: [] });
    }
  });
};

const getDetailsFromGraph = (uri, graph, type, preflanguages) => {
  let props = {
    labels: [
      rdflib.sym("http://www.w3.org/2000/01/rdf-schema#label"),
      rdflib.sym("http://ogp.me/ns#title"),
      rdflib.sym("http://purl.org/dc/elements/1.1/title"),
    ],
    comments: [
      rdflib.sym("http://www.w3.org/2000/01/rdf-schema#comment"),
      rdflib.sym("http://ogp.me/ns#description"),
      rdflib.sym("http://purl.org/dc/elements/1.1/description"),
    ],
  };
  let res = [];
  props[type].forEach((prop) => {
    let items = graph.statementsMatching(rdflib.sym(uri), prop, undefined);
    //console.log('test', preflanguages)
    let resprop = undefined;
    let count = 0;
    //console.log(items)
    if (items.length > 0) {
      while (!resprop) {
        if (count <= preflanguages.length - 1) {
          resprop = items.find((it) => it.object.lang === preflanguages[count]);
          //console.log('find', resprop.object.value)
        } else {
          resprop = items[0];
          //console.log('unique', resprop)
        }
        count++;
      }
      //console.log('test apres', resprop)
      res.push(resprop.object.value);
    }
  });
  //console.log('test languages', res)
  return res;
};

exports.getLabels = getLabels;
exports.getDetailsFromGraph = getDetailsFromGraph;
